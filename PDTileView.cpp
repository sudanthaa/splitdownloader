// PDTileView.cpp : implementation file
//

#include "PDPch.h"
#include <math.h>
#include "PDTileView.h"
#include "PDDialog.h"
#include "PDItem.h"
#include "PDDownloadUnit.h"

// PDTileView

IMPLEMENT_DYNAMIC(PDTileView, CWnd)

PDTileView::PDTileView(PDDialog* pDlg, CRect rRect)
:p_Dlg(pDlg)
{
	Create(NULL, NULL, WS_CHILD|WS_VISIBLE|WS_BORDER, rRect, pDlg, 10013);
}

PDTileView::~PDTileView()
{
}


BEGIN_MESSAGE_MAP(PDTileView, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// PDTileView message handlers



void PDTileView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CWnd::OnPaint() for painting messages

	PDItem* pItem = p_Dlg->GetItem();
	if (!pItem)
		return;

	LST_UNIT* plstUnits = pItem->GetUnits();
	if (plstUnits->size() == 0)
		return;

	CRect rRect;
	GetClientRect(rRect);

	int iPixels = (rRect.Width() -1)  * (rRect.Height() - 1);
	int iPixPerTile = iPixels / plstUnits->size();
	double dPixPerTile = (double)iPixPerTile;
	float dSideLen = (float)sqrt(dPixPerTile);

	int iSize = (int)dSideLen;
	iSize = min(iSize, 8); // Nice size. Too large is ugly
	int iColumns = rRect.Width()/iSize;


	int iIndex = 0;
	LST_UNIT_ITR itr = plstUnits->begin();
	for (; itr != plstUnits->end(); itr++)
	{
		PDDownloadUnit* pUnit = *itr;
		int iState = pUnit->GetState();

		int iRow = iIndex/iColumns;
		int iColumn = iIndex%iColumns;

		COLORREF cr = 0;
		if		(iState == ITEM_STATE_IDLE)	cr = RGB(200,200,200);
		else if	(iState == ITEM_STATE_DOWNLOADING)	cr = RGB(200,150,150);
		else if	(iState == ITEM_STATE_COMPLETE)	cr = RGB(150,200,150);

		CRect rTile;
		rTile.left = iColumn * iSize + 1;
		rTile.top = iRow * iSize + 1;
		rTile.right = rTile.left + iSize - 1;
		rTile.bottom = rTile.top + iSize - 1;

		dc.FillSolidRect(rTile, cr);

		iIndex++;
	}
}

void PDTileView::Reset()
{
	CClientDC dcClient(this);

	CRect rRect;
	GetClientRect(rRect);
	dcClient.FillSolidRect(rRect, RGB(255,255,255));
}
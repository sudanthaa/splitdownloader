#pragma once


// PDProgressBar

class PDProgressBar : public CProgressCtrl
{
	DECLARE_DYNAMIC(PDProgressBar)

public:
	PDProgressBar();
	virtual ~PDProgressBar();

	CBrush o_BackBrush;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
};



// PartialDownloaderDlg.cpp : implementation file
//

#include "PDPch.h"
#include "PDApp.h"
#include "PDDialog.h"

#include <Ws2bth.h>
#include <WinInet.h>
#include <Shlwapi.h>
#include <BluetoothAPIs.h>
#include "PDHTMLResponse.h"
#include "PDDownloadThread.h"
#include "PDDownloadUnit.h"
#include "PDItem.h"
#include "PDConfig.h"
#include "PDTileView.h"
#include "PDStatusBar.h"
#include "PDFileMergeDialog.h"
#include "PDLogger.h"


#define  HEADER_BUF_SIZE 5000
#define  ID_TIMER_PROCESS   11000
#define  MAX_THREADS	9
#define  PROGRESS_BAR_HEIGHT	16
#define  LEN_URL  2000
#define  MAX_STAT_ENTRIES	10

#define  READ_BUFF_SIZE 2000

#define  WS_CNTLEN_TAG_SEARCH	"Content-Length:"
#define  WS_CNTLEN_PREFIX_TAG	"<td>"
#define  WS_CNTLEN_SUFFIX_TAG	"</td>"

#define  FWS_CNTLEN_TAG_SEARCH	"RETURN-CONTENT-SIZE"
#define  FWS_CNTLEN_PREFIX_TAG	"</b>"
#define  FWS_CNTLEN_SUFFIX_TAG	"<hr>"

#define  WS_REFER " You may refer http://web-sniffer.net aquire file size. "\
				"Generate a GET request, which will return the file size, in the property called \"Content-Length\"."

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About
//**************************************************************************************************
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

//**************************************************************************************************
CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

//**************************************************************************************************
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// PDDialog dialog



//**************************************************************************************************
PDDialog::PDDialog(CWnd* pParent /*=NULL*/)
	: CDialog(PDDialog::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	p_Item = NULL;
	i_TheardCount = 0;
	a_Threads = NULL;
	p_TileView = NULL;
}

//**************************************************************************************************
PDDialog::~PDDialog()
{
	delete p_StatusBar;
	p_StatusBar = NULL;

	delete p_TileView;
	p_TileView = NULL;
}


//**************************************************************************************************
void PDDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PROXY_SERVER, o_EditProxyServer);
	DDX_Control(pDX, IDC_EDIT_PROXY_PORT, o_EditProxyPort);
	DDX_Control(pDX, IDC_EDIT_THREADS, o_EditThreadCount);
	DDX_Control(pDX, IDC_EDIT_URL, o_EditURL);
	DDX_Control(pDX, IDC_EDIT_BLOCK_SIZE, o_EditBlockSize);
	DDX_Control(pDX, IDC_CHECK_RECOVER, o_CheckRecover);
	DDX_Control(pDX, IDC_EDIT_RECOVER_ID, o_EditRecoverID);
	DDX_Control(pDX, IDC_STATIC_BLOCK_SIZE_MB, o_StaticBlockSizeMB);
	DDX_Control(pDX, IDC_CHECK_CLEAN_FILES, o_CheckCleanFiles);
}

//**************************************************************************************************

BEGIN_MESSAGE_MAP(PDDialog, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_TEST, &PDDialog::OnBnClickedButtonStart)
	ON_WM_TIMER()
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_THREADS, &PDDialog::OnDeltaposSpinThreads)
	ON_EN_CHANGE(IDC_EDIT_PROXY_SERVER, &PDDialog::OnEnChangeEditProxyServer)
	ON_EN_CHANGE(IDC_EDIT_PROXY_PORT, &PDDialog::OnEnChangeEditProxyPort)
	ON_BN_CLICKED(IDC_CHECK_USE_PROXY, &PDDialog::OnBnClickedCheckUseProxy)
	ON_EN_CHANGE(IDC_EDIT_URL, &PDDialog::OnEnChangeURL)
	ON_EN_CHANGE(IDC_EDIT_BLOCK_SIZE, &PDDialog::OnEnChangeEditBlockSize)
	ON_WM_CLOSE()
	ON_EN_CHANGE(IDC_EDIT_RECOVER_ID, &PDDialog::OnEnChangeEditRecoverId)
	ON_BN_CLICKED(IDC_CHECK_RECOVER, &PDDialog::OnBnClickedCheckRecover)
	ON_BN_CLICKED(IDC_CHECK_CLEAN_FILES, &PDDialog::OnBnClickedCheckCleanFiles)
	ON_BN_CLICKED(IDC_BUTTON_DOWNLOADS_DIR, &PDDialog::OnBnClickedButtonDownloadsDir)
END_MESSAGE_MAP()


// PDDialog message handlers

//**************************************************************************************************
BOOL PDDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	CFG()->LoadFromRegistry();
	UpdateFieldsFromConfig();
	InitTileView();
	InitStatusBar();
	SetThreadCount(CFG()->i_Threads);
	SetTimer(ID_TIMER_PROCESS, 1000, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

//**************************************************************************************************
void PDDialog::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
//**************************************************************************************************
void PDDialog::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
//**************************************************************************************************
HCURSOR PDDialog::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//**************************************************************************************************
void PDDialog::OnBnClickedButtonStart()
{
	// TODO: Add your control notification handler code herF

	if (CFG()->s_URL.GetLength() == 0)
	{
		AfxMessageBox("URL is not set");
		return;
	}

	PDItem* pItem = new PDItem;
	if (!pItem->Init(CFG()->s_URL))
	{
		AfxMessageBox("Failed decode URL");
		TRACE("Failed decode item \n");
		delete pItem;
		pItem = NULL;
	}

	if (!GetFileSizeFromSmallSizePartialRequest(pItem))
	{
		AfxMessageBox("Failed to find fiGetFileSizeFromSmallSizePartialRequestle size thought Range request. "
			"Please define the file size if problem continues to exist." WS_REFER);
		delete pItem;
		return;
	}

	ScheduleItem(pItem);
}

//**************************************************************************************************
void PDDialog::ScheduleItem( PDItem* pItem )
{
	TRACE("ShedulingItem. id=%s\n", pItem->s_ID);
	int iContentSize = pItem->GetSize();
	int iUnits = (iContentSize / CFG()->i_DownloadUnitSize) + 1;
	if ((iContentSize % CFG()->i_DownloadUnitSize) == 0)
		iUnits--;


	for (int i = 0; i < iUnits; i++)
	{
		int iStart = i * CFG()->i_DownloadUnitSize;
		int iSize = 0;
		
		if (i == iUnits - 1)
			iSize =  iContentSize - iStart;
		else 
			iSize = CFG()->i_DownloadUnitSize;

		PDDownloadUnit* pUnit = new PDDownloadUnit(pItem, iStart, iSize);
		pItem->AddUnit(pUnit);
	}

	p_Item = pItem;
	p_TileView->Reset();
}


//**************************************************************************************************
void PDDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent = ID_TIMER_PROCESS)
		OnProcessTimer();

	CDialog::OnTimer(nIDEvent);
}

//**************************************************************************************************
void PDDialog::OnProcessTimer()
{
	if (!p_Item)
	{
		for (int i = 0; i < i_TheardCount; i++)
		{
			PDDownloadThread* pThread = a_Threads[i];
			pThread->Refresh();
		}
		return;
	}

	UpdateStat();

	for (int i = 0; i < i_TheardCount; i++)
	{
		PDDownloadThread* pThread = a_Threads[i];
		PDDownloadUnit* pUnit = pThread->GetUnit();
		
		bool bNeedAssign = false;
		if (!pUnit)
			bNeedAssign = true;
		else 
		{
			if (pUnit->IsComplete())
			{
				p_StatusBar->SetMsg("Downloading..");

				pThread->SetUnit(NULL);
				p_Item->AddCompletedUnit(pUnit);

				if (p_Item->IsAllComplete())
				{
					TRACE("All downloads are complete\n");
					p_StatusBar->SetMsg("All units are completed. Finalizing...");
					
					
					//p_Item->Finalize();

					PDFileMergeDialog dlg;
					dlg.SetItem(p_Item);
					dlg.DoModal();



					p_Item = NULL;

					p_StatusBar->SetMsg("Idle...");
					break;
				}
				else
					bNeedAssign = true;
			}
		}

		if (bNeedAssign)
		{
			if (p_Item->HasIncomplete())
			{
				PDDownloadUnit* pUnit = p_Item->PopIncomplete();
				pThread->SetUnit(pUnit);
				pThread->Execute();
			}
		}

		pThread->Refresh();
	}

	//// Update progress
	//CMainFrame* mainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	//mainFrm->SetProgressBarPosition(10);

	p_TileView->Invalidate();
}


//**************************************************************************************************
bool PDDialog::SetThreadCount( int iThreads )
{
	if (p_Item)
	{
		TRACE("Not allowed to alter thread count while item is being downloaded");
		return false;
	}

	for (int i = 0; i < i_TheardCount; i++)
	{
		PDDownloadThread* pThread = a_Threads[i];
		delete pThread;
	}

	delete [] a_Threads;
	a_Threads = NULL;
	i_TheardCount = 0;

	i_TheardCount = iThreads;
	a_Threads = new PDDownloadThread*[i_TheardCount];

	for (int i = 0; i < i_TheardCount; i++)
		a_Threads[i] = new PDDownloadThread(i, this);

	return true;
}

//**************************************************************************************************
void PDDialog::GetProgressArea( CRect& rRect, int iIndex)
{
	CWnd* pWnd = GetDlgItem(IDC_STATIC_PROGRESS_AREA);

	CRect rTmp;
	pWnd->GetWindowRect(rTmp);

	ScreenToClient(rTmp);

	int iHeight = PROGRESS_BAR_HEIGHT;
	rRect.right = rTmp.right;
	rRect.left = rTmp.left;
	rRect.top = rTmp.top + iIndex * iHeight;
	rRect.bottom = rTmp.top + (iIndex + 1) * iHeight;

	// Keep gap
	rRect.top += 3;
	rRect.bottom -= 3;
}

//**************************************************************************************************
bool PDDialog::GetFileSizeFromSmallSizePartialRequest(PDItem* pItem)
{
	CString sProxyConn;
	if (pItem->e_Scheme == INTERNET_SCHEME_HTTP)
		sProxyConn.Format("http=http://%s:%d", CFG()->s_ProxyServer, CFG()->i_ProxyPort);
	else if (pItem->e_Scheme == INTERNET_SCHEME_HTTPS)
		sProxyConn.Format("https=https://%s:%d", CFG()->s_ProxyServer, CFG()->i_ProxyPort);

	p_StatusBar->SetMsg("Opening internet connection.");
	HINTERNET hInet = InternetOpen("Part Downloader",
		//CFG()->b_UseProxy ? INTERNET_OPEN_TYPE_PROXY :INTERNET_OPEN_TYPE_PRECONFIG,
		INTERNET_OPEN_TYPE_PRECONFIG,
		CFG()->b_UseProxy ? sProxyConn: NULL, 
		NULL,0);

	p_StatusBar->SetMsg("Connecting to internet.");
	HINTERNET hServer = InternetConnect(hInet, pItem->s_Server, pItem->i_Port,
		NULL, NULL, INTERNET_SERVICE_HTTP, 0, NULL);

	DWORD dwFlags = INTERNET_FLAG_KEEP_CONNECTION;
	if (pItem->e_Scheme == INTERNET_SCHEME_HTTPS)
		dwFlags |= INTERNET_FLAG_SECURE;

	CString sPath = pItem->s_FilePath;
	if (!pItem->s_ExtraInfo.IsEmpty())
		sPath += pItem->s_ExtraInfo;

	p_StatusBar->SetMsg("Opening http request");
	HINTERNET hHttp = HttpOpenRequest(hServer, TEXT("GET"),
		//TEXT("/"),
		sPath,
		"HTTP/1.1", NULL, NULL, 
		dwFlags, 0);

	//{
	//	DWORD dwFlags;
	//	DWORD dwBuffLen = sizeof(dwFlags);

	//	InternetQueryOption (hHttp, INTERNET_OPTION_SECURITY_FLAGS,
	//		(LPVOID)&dwFlags, &dwBuffLen);

	//	dwFlags |= SECURITY_FLAG_IGNORE_UNKNOWN_CA;
	//	dwFlags |= SECURITY_FLAG_IGNORE_CERT_CN_INVALID;
	//	InternetSetOption (hHttp, INTERNET_OPTION_SECURITY_FLAGS,
	//		&dwFlags, sizeof (dwFlags) );
	//}


 	CString sExtraHeaders;
 	sExtraHeaders.Format("Range: bytes=0-99");
	BOOL bSuccess = HttpSendRequest(hHttp, sExtraHeaders, -1, NULL, 0);
	if (!bSuccess)
	{
		 return false;
	}

	// Above will success after request is sent. Next is to read the response
	//////////////////////////////////////////////////////////////////////////

	int iResultBufSize = 10000;
	DWORD dwSize = iResultBufSize;	
	char* lpvResultHeaderBuffer = new char[dwSize];
	BOOL bRet = HttpQueryInfo(hHttp, HTTP_QUERY_RAW_HEADERS_CRLF,
		lpvResultHeaderBuffer, &dwSize, NULL);

	TRACE("%s\n", lpvResultHeaderBuffer);
	PDHTMLResponse* pRes = new PDHTMLResponse;
	if (!pRes->Init(lpvResultHeaderBuffer, dwSize))
	{
		InternetCloseHandle(hHttp);
		InternetCloseHandle(hServer);
		InternetCloseHandle(hInet);

		delete [] lpvResultHeaderBuffer;
		return false;
	}

	pItem->SetSize(pRes->GetContentSizeFromContentRange());

	CString sHeadSizeFileName;
	sHeadSizeFileName.Format("SampleData_%s.htm", pItem->s_ID);
	CFile oFile;
	oFile.Open(sHeadSizeFileName, CFile::modeWrite | CFile::modeCreate);

	DWORD dwPayloadSize = 20000;
	char* pPayLoadBuffer = new char[dwPayloadSize];
	DWORD dwPayloadRead = 0;
	BOOL bDataRes = InternetReadFile(hHttp, pPayLoadBuffer, dwPayloadSize, &dwPayloadRead);
	TRACE("%s\n", pPayLoadBuffer);
	oFile.Write(pPayLoadBuffer, dwPayloadRead);
	
	*pPayLoadBuffer = 0;

	while (dwPayloadRead > 0)
	{	
		BOOL bDataRes = InternetReadFile(hHttp, pPayLoadBuffer, dwPayloadSize, &dwPayloadRead);
		TRACE("%s\n", pPayLoadBuffer);
		oFile.Write(pPayLoadBuffer, dwPayloadRead);
		*pPayLoadBuffer = 0;
	}

	oFile.Flush();
	oFile.Close();

	InternetCloseHandle(hHttp);
	InternetCloseHandle(hServer);
	InternetCloseHandle(hInet);
	return true;
}

//**************************************************************************************************
void PDDialog::OnDeltaposSpinThreads(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	bool bInc = (pNMUpDown->iDelta < 0);
	if (bInc)
	{
		if (CFG()->i_Threads < MAX_THREADS)
		{
			if (SetThreadCount(CFG()->i_Threads + 1))
			{
				CFG()->i_Threads++;
				CString sValue;
				sValue.Format("%d", CFG()->i_Threads);
				o_EditThreadCount.SetWindowText(sValue);
			}
		}	
	}
	else 
	{
		if (CFG()->i_Threads > 1)
		{
			if (SetThreadCount(CFG()->i_Threads - 1))
			{
				CFG()->i_Threads--;
				CString sValue;
				sValue.Format("%d", CFG()->i_Threads);
				o_EditThreadCount.SetWindowText(sValue);
			}
		}
	}
}

////**************************************************************************************************
//void PDDialog::OnEnChangeEditFileSize()
//{
//	// TODO:  If this is a RICHEDIT control, the control will not
//	// send this notification unless you override the CDialog::OnInitDialog()
//	// function and call CRichEditCtrl().SetEventMask()
//	// with the ENM_CHANGE flag ORed into the mask.
//
//	// TODO:  Add your control notification handler code here
//
//
//	//CString sValue;
//	//o_EditFileSize.GetWindowText(sValue);
//	CFG()->i_FileSize = atoi(sValue);
//}

//**************************************************************************************************
void PDDialog::OnEnChangeEditProxyServer()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	CString sValue;
	o_EditProxyServer.GetWindowText(sValue);
	CFG()->s_ProxyServer = sValue;
}

//**************************************************************************************************
void PDDialog::OnEnChangeEditProxyPort()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	CString sValue;
	o_EditProxyPort.GetWindowText(sValue);
	CFG()->i_ProxyPort = atoi(sValue);
}

////**************************************************************************************************
//void PDDialog::OnBnClickedCheckDefineLength()
//{
//	// TODO: Add your control notification handler code here
//
//	CFG()->b_DefineSize = o_CheckDefineSize.GetCheck() > 0 ;
//	o_EditFileSize.EnableWindow(CFG()->b_DefineSize? TRUE : FALSE);
//}

//**************************************************************************************************
void PDDialog::OnBnClickedCheckUseProxy()
{
	// TODO: Add your control notification handler code here

	// CFG()->b_UseProxy = o_CheckUseProxy.GetCheck() > 0 ;
	o_EditProxyServer.EnableWindow(CFG()->b_UseProxy ? TRUE : FALSE);
	o_EditProxyPort.EnableWindow(CFG()->b_UseProxy ? TRUE : FALSE);

}

//**************************************************************************************************
void PDDialog::OnBnClickedCheckRecover()
{
	// TODO: Add your control notification handler code here
	CFG()->b_Recover = o_CheckRecover.GetCheck() > 0 ;
	o_EditRecoverID.EnableWindow(CFG()->b_Recover ? TRUE : FALSE);
}


//**************************************************************************************************
void PDDialog::UpdateFieldsFromConfig()
{
	CString sValue;
	
	sValue.Format("%d", CFG()->i_ProxyPort);
	o_EditProxyPort.SetWindowText(sValue);

	sValue.Format("%d", CFG()->i_Threads);
	o_EditThreadCount.SetWindowText(sValue);

	sValue.Format("%d", CFG()->i_DownloadUnitSize);
	o_EditBlockSize.SetWindowText(sValue);

	double dVal = (double)CFG()->i_DownloadUnitSize;
	sValue.Format("%.3f MB", dVal / (1024.0 * 1024.0));
	o_StaticBlockSizeMB.SetWindowText(sValue);

	o_EditProxyServer.SetWindowText(CFG()->s_ProxyServer);
	o_EditURL.SetWindowText(CFG()->s_URL);
	o_EditRecoverID.SetWindowText(CFG()->s_RecoverID);
	o_CheckRecover.SetCheck(CFG()->b_Recover ? 1:0);
	o_CheckCleanFiles.SetCheck(CFG()->b_CleanFiles ? 1: 0);

	o_EditProxyServer.EnableWindow(CFG()->b_UseProxy ? TRUE : FALSE);
	o_EditProxyPort.EnableWindow(CFG()->b_UseProxy ? TRUE : FALSE);
	o_EditRecoverID.EnableWindow(CFG()->b_Recover? TRUE : FALSE);
}

//**************************************************************************************************
void PDDialog::OnEnChangeURL()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	
	CString sValue;
	o_EditURL.GetWindowText(sValue);
	CFG()->s_URL = sValue;
}

//**************************************************************************************************
void PDDialog::Merge()
{
	//// nds1_nokia_com_l_phones_software_Nokia_Suite_webinstaller_ALL_exe__20120213-140445_0-4000000

	//int iCount = 24;

	//int iBlockSize = 200000;
	//char* pBuf = new char[iBlockSize];

	//CFile oFile;
	//CFileException ex;
	//if (!oFile.Open("NS.exe", CFile::modeWrite | CFile::modeCreate, &ex))
	//{
	//	TCHAR szError[1024];
	//	ex.GetErrorMessage(szError, 1024);
	//	TRACE("File open error. [%s], err=%s", "NS.exe", szError);
	//	return;
	//}


	//for (int i = 0 ; i < iCount; i++)
	//{
	//	CString sFileName;
	//	sFileName.Format("nds1_nokia_com_l_phones_software_Nokia_Suite_webinstaller_ALL_exe__20120213-134900_%d-%d.dat"
	//		, i * CFG()->i_DownloadUnitSize,   CFG()->i_DownloadUnitSize);

	//	CFile oUnitFile;
	//	CFileException ex2;
	//	if (!oUnitFile.Open(sFileName, CFile::modeRead, &ex2))
	//	{
	//		TCHAR szError[1024];
	//		ex2.GetErrorMessage(szError, 1024);
	//		TRACE("File open error. [%s], err=%s",sFileName, szError);
	//		return;
	//	}

	//	int iRead = oUnitFile.Read(pBuf, iBlockSize);
	//	while (iRead > 0)
	//	{
	//		oFile.Write(pBuf, iRead);
	//		iRead = oUnitFile.Read(pBuf, iBlockSize);
	//	}

	//	oUnitFile.Close();
	//}

	//oFile.Flush();
	//oFile.Close();
}


void PDDialog::ConvertToWebHashString( const char* zString, char * zOutBuff,int iOutBufLen )
{
	int iPostLen = 0;
	CString sPutRequest;

	const char* zURL = zString;
	int iTmpBuffLen = (strlen(zURL) * 3) + 1;
	char* zURLConv = new char[iTmpBuffLen];

	char* zItrDes = zURLConv;
	const char* zItrSrc = zURL;
	*zItrDes = 0;
	int iDes = 0;

	while (*zItrSrc != 0)
	{
		if		(*zItrSrc == ':')	{	strcpy_s(zItrDes, iTmpBuffLen - iDes, "%3A"); zItrDes += 3; iDes += 3; }
		else if	(*zItrSrc == '/')	{	strcpy_s(zItrDes, iTmpBuffLen - iDes, "%2F"); zItrDes += 3; iDes += 3; }
		else					{	*zItrDes = *zItrSrc;	zItrDes++; iDes++; }

		zItrSrc++;
	}
	*zItrDes = 0;

	sprintf_s(zOutBuff, iOutBufLen, "%s", zURLConv);
}

//**************************************************************************************************
void PDDialog::InitTileView()
{
	CWnd* pWnd = GetDlgItem(IDC_STATIC_TILE_VIEW_AREA);
	CRect rSV;
	pWnd->GetWindowRect(rSV);
	ScreenToClient(rSV);
	p_TileView = new PDTileView(this, rSV);
}

//**************************************************************************************************
void PDDialog::InitStatusBar()
{
	CWnd* pWnd = GetDlgItem(IDC_STATIC_STATUS_AREA);
	CRect rSV;
	pWnd->GetWindowRect(rSV);
	ScreenToClient(rSV);
	p_StatusBar = new PDStatusBar(this, rSV);
	p_StatusBar->Invalidate();
}

//**************************************************************************************************

void PDDialog::OnEnChangeEditBlockSize()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	CString sValue;
	o_EditBlockSize.GetWindowText(sValue);
	CFG()->i_DownloadUnitSize = atoi(sValue);

	double dVal = (double)CFG()->i_DownloadUnitSize;
	sValue.Format("%.3f MB", dVal / (1024.0 * 1024.0));
	o_StaticBlockSizeMB.SetWindowText(sValue);
}

//**************************************************************************************************
void PDDialog::OnEnChangeEditRecoverId()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	o_EditRecoverID.GetWindowText(CFG()->s_RecoverID);
}

//**************************************************************************************************
void PDDialog::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	CFG()->SetToRegistry();
	JoinAndDeleteThreads();
	
	CDialog::OnClose();
}


//**************************************************************************************************
void PDDialog::UpdateStat()
{
	// Update stat collector.
	lst_ProgressAtSec.push_back(p_Item->GetTotalProgress());
	if (lst_ProgressAtSec.size() > MAX_STAT_ENTRIES)
		lst_ProgressAtSec.pop_front();

	// Set stat

	p_StatusBar->SetElapsed(p_Item->GetElapsed());

	if (lst_ProgressAtSec.size() > 1 )
	{
		int iTotalByteDiff = lst_ProgressAtSec.back() - lst_ProgressAtSec.front();
		int iCount = (int)(lst_ProgressAtSec.size());
		iCount--;
		int iBytesPerSec = iTotalByteDiff / iCount;
		p_StatusBar->SetSpeed(iBytesPerSec);

		int iTotalLeft = p_Item->GetSize() - p_Item->GetTotalProgress();
		if (iBytesPerSec > 0)
		{
			int iETA = iTotalLeft / iBytesPerSec;
			p_StatusBar->SetETA(iETA);
		}
	}

	p_StatusBar->Invalidate();
}

//**************************************************************************************************
void PDDialog::JoinAndDeleteThreads()
{
	if (p_Item)
		p_Item->Cancel();

	for (int i = 0; i < i_TheardCount; i++)
	{
		PDDownloadThread* pThread = a_Threads[i];
		delete pThread;
		a_Threads[i] = NULL;
	}

	delete [] a_Threads;
	a_Threads = NULL;
	i_TheardCount = 0;
}

//**************************************************************************************************


void PDDialog::OnBnClickedCheckCleanFiles()
{
	// TODO: Add your control notification handler code here
	CFG()->b_CleanFiles= o_CheckCleanFiles.GetCheck() > 0 ;
}

//**************************************************************************************************
void PDDialog::EnumerateDevices()
{
	BLUETOOTH_FIND_RADIO_PARAMS m_bt_find_radio = {sizeof(BLUETOOTH_FIND_RADIO_PARAMS)};
	BLUETOOTH_RADIO_INFO m_bt_info = {sizeof(BLUETOOTH_RADIO_INFO),0,};
	BLUETOOTH_DEVICE_SEARCH_PARAMS m_search_params = {
		sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS),1,0,1,1,1,15,NULL};

	BLUETOOTH_DEVICE_INFO m_device_info = {sizeof(BLUETOOTH_DEVICE_INFO),0,};

	HANDLE m_radio = NULL;
	HBLUETOOTH_RADIO_FIND m_bt = NULL;
	HBLUETOOTH_DEVICE_FIND m_bt_dev = NULL;
	int m_radio_id;
	int m_device_id;
	DWORD mbtinfo_ret;

	// Iterate for available bluetooth radio devices in range
	// Starting from the local
	while (TRUE)
	{
		m_bt = BluetoothFindFirstRadio(&m_bt_find_radio, &m_radio);
		if (m_bt != NULL)
			TRACE("BluetoothFindFirstRadio() is OK!\n");
		else
			TRACE("BluetoothFindFirstRadio() failed with error code %d\n", GetLastError());

		m_radio_id = 0;
		do {

			// Then get the radio device info....
			mbtinfo_ret = BluetoothGetRadioInfo(m_radio, &m_bt_info);
			if (mbtinfo_ret == ERROR_SUCCESS)
				TRACE("BluetoothGetRadioInfo() looks fine!\n");
			else
				TRACE("BluetoothGetRadioInfo() failed wit herror code %d\n", mbtinfo_ret);

			TRACE("Radio %d:\r\n", m_radio_id);
			TRACE("\tInstance Name: %s\r\n", m_bt_info.szName);
			TRACE("\tAddress: %02X:%02X:%02X:%02X:%02X:%02X\r\n", 
				m_bt_info.address.rgBytes[5], m_bt_info.address.rgBytes[4], 
				m_bt_info.address.rgBytes[3], m_bt_info.address.rgBytes[2],
				m_bt_info.address.rgBytes[1], m_bt_info.address.rgBytes[0]);

			TRACE("\tClass: 0x%08x\r\n", m_bt_info.ulClassofDevice);
			TRACE("\tManufacturer: 0x%04x\r\n", m_bt_info.manufacturer);

			m_search_params.hRadio = m_radio;
			ZeroMemory(&m_device_info, sizeof(BLUETOOTH_DEVICE_INFO));
			m_device_info.dwSize = sizeof(BLUETOOTH_DEVICE_INFO);

			// Next for every radio, get the device
			m_bt_dev = BluetoothFindFirstDevice(&m_search_params, &m_device_info);

			if (m_bt_dev != NULL)
				TRACE("\nBluetoothFindFirstDevice() is working!\n");
			else
				TRACE("\nBluetoothFindFirstDevice() failed with error code %d\n", GetLastError());

			m_radio_id++;
			m_device_id = 0;

			// Get the device info
			do
			{
				TRACE("\n\tDevice %d:\r\n", m_device_id);
				TRACE("  \tInstance Name: %s\r\n", m_device_info.szName);
				TRACE("  \tAddress: %02X:%02X:%02X:%02X:%02X:%02X\r\n", 
					m_device_info.Address.rgBytes[5], m_device_info.Address.rgBytes[4], 
					m_device_info.Address.rgBytes[3], m_device_info.Address.rgBytes[2],
					m_device_info.Address.rgBytes[1], m_device_info.Address.rgBytes[0]);

				TRACE("  \tClass: 0x%08x\r\n", m_device_info.ulClassofDevice);
				TRACE("  \tConnected: %s\r\n", m_device_info.fConnected ? L"true" : L"false");
				TRACE("  \tAuthenticated: %s\r\n", m_device_info.fAuthenticated ? L"true" : L"false");
				TRACE("  \tRemembered: %s\r\n", m_device_info.fRemembered ? L"true" : L"false");

				m_device_id++;

				// Well, the found device information can be used for further socket
				// operation such as creating a socket, bind, listen, connect, send, receive etc..
				// If no more device, exit the loop
				if (!BluetoothFindNextDevice(m_bt_dev, &m_device_info))
					break;

			} while(BluetoothFindNextDevice(m_bt_dev, &m_device_info));

			// NO more device, close the device handle
			if(BluetoothFindDeviceClose(m_bt_dev) == TRUE)
				TRACE("\nBluetoothFindDeviceClose(m_bt_dev) is OK!\n");
			else
				TRACE("\nBluetoothFindDeviceClose(m_bt_dev) failed with error code %d\n", GetLastError());

		} while(BluetoothFindNextRadio(&m_bt_find_radio, &m_radio));

		// No more radio, close the radio handle
		if (BluetoothFindRadioClose(m_bt) == TRUE)
			TRACE("BluetoothFindRadioClose(m_bt) is OK!\n");
		else
			TRACE("BluetoothFindRadioClose(m_bt) failed with error code %d\n", GetLastError());

		// Exit the outermost WHILE and BluetoothFindXXXXRadio loops if there is no more radio
		if (!BluetoothFindNextRadio(&m_bt_find_radio, &m_radio))
			break;

		// Give some time for the 'signal' which is a typical for crap wireless devices
		Sleep(1000);
	}
}

//**************************************************************************************************
void PDDialog::EnumerateDeviceEx(std::vector<BtDevice*>& aList)
{
	aList.clear();

	WSAQUERYSET wsaq;
	HANDLE hLookup;
	union {
		CHAR buf[5000];
		double __unused; // ensure proper alignment
	};
	LPWSAQUERYSET pwsaResults = (LPWSAQUERYSET) buf;
	DWORD dwSize  = sizeof(buf);
	BOOL bHaveName;
	ZeroMemory(&wsaq, sizeof(wsaq));
	wsaq.dwSize = sizeof(wsaq);
	wsaq.dwNameSpace = NS_BTH;
	wsaq.lpcsaBuffer = NULL;
	if (ERROR_SUCCESS != WSALookupServiceBegin (&wsaq, LUP_CONTAINERS, &hLookup))
	{
		TRACE("WSALookupServiceBegin failed %d\r\n", GetLastError());
		return;
	}
	ZeroMemory(pwsaResults, sizeof(WSAQUERYSET));
	pwsaResults->dwSize = sizeof(WSAQUERYSET);
	pwsaResults->dwNameSpace = NS_BTH;
	pwsaResults->lpBlob = NULL;
	while (ERROR_SUCCESS == WSALookupServiceNext (hLookup, LUP_RETURN_NAME | LUP_RETURN_ADDR, 
		&dwSize, pwsaResults))
	{

		ASSERT (pwsaResults->dwNumberOfCsAddrs == 1);
		BTH_ADDR b = ((SOCKADDR_BTH *)pwsaResults->lpcsaBuffer->RemoteAddr.lpSockaddr)->btAddr;
		bHaveName = pwsaResults->lpszServiceInstanceName && *(pwsaResults->lpszServiceInstanceName);
		BtDevice* pDev = new BtDevice;
		pDev->s_Name = bHaveName ? pwsaResults->lpszServiceInstanceName : "";
		pDev->addr_Device = b;
		aList.push_back(pDev);

		TRACE("%s%s%04x%08x%s\n", bHaveName ? pwsaResults->lpszServiceInstanceName : "", 
			bHaveName ? "(" : "", GET_NAP(b), GET_SAP(b), bHaveName ? ")" : "");
	}
	WSALookupServiceEnd(hLookup);
}

//**************************************************************************************************
void PDDialog::OnBnClickedButtonDownloadsDir()
{
	// TODO: Add your control notification handler code here
	ShellExecute(NULL, "open", PDConfig::p_Inst->s_DownloadsDirectory, NULL, NULL, SW_SHOWNORMAL);
}


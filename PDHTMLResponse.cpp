#include "PDPch.h"
#include "PDHTMLResponse.h"
#include "PDLogger.h"

#define  HTTP_RESPONSE_END  "\r\n\r\n"

PDHTMLResponse::PDHTMLResponse(void)
{
	i_HeaderSize = 0;
	pz_DeficitBuff = NULL;
	i_DeficitSize = 0;

	s_RangeType = "";
	i_RangeStart = -1;
	i_RangeEnd = -1;
	i_RangeFullSize = -1;
}

PDHTMLResponse::~PDHTMLResponse(void)
{
}

bool PDHTMLResponse::Init( const char* zStr, int iSize)
{
	const char* zEnd = strstr(zStr, HTTP_RESPONSE_END);
	if (!zEnd)
		return false;
	char t1 = '\r';
	char t2 = '\n';

	int iHeadSize = sizeof(HTTP_RESPONSE_END);

	i_HeaderSize = (zEnd - zStr) + sizeof(HTTP_RESPONSE_END) - 1;
	char* pzHeaderString = new char[i_HeaderSize + 1];
	
	i_DeficitSize = iSize - i_HeaderSize;
	pz_DeficitBuff = new char[iSize - i_HeaderSize];

	memcpy(pzHeaderString, zStr, i_HeaderSize + 1);
	memcpy(pz_DeficitBuff, zStr + i_HeaderSize, i_DeficitSize);

	pzHeaderString[i_HeaderSize] = 0;
	PDLog("Response....\n%s", pzHeaderString);


	const char* zLineSep = "\r\n";
	const char* zWordSep = ":";
	const char* zWordSepFL = " ";

	char* zNextLine = NULL;
	int iLine = 0;

	for (char* zLine = strtok_s(pzHeaderString, zLineSep, &zNextLine); 
			zLine ; zLine = strtok_s( NULL, zLineSep, &zNextLine))
	{
		char* zNextWord = NULL;

		const char* zWSep = (iLine == 0) ? zWordSepFL : zWordSep;

		int iWord = 0;
		CString sKey = "";
		for (char* zWord = strtok_s(zLine, zWSep, &zNextWord); 
			zWord ; zWord = strtok_s( NULL, zWSep, &zNextWord))
		{
			CString sWord = zWord;
			sWord.Trim();

			if (iLine == 0)
			{
				if		(iWord == 0)	s_Version = zWord;
				else if (iWord == 1)	s_Response = zWord;
				else if (iWord == 2)	s_ResponseReason = zWord;
			}
			else
			{
				if	(iWord == 0)	sKey = sWord;
				else				map_Params[sKey] = sWord;
			}

			iWord++;
		}
		iLine++;
	}

	//Content-Range: bytes 0-999/3980

	CString sRange = GetStrParam("Content-Range");
	if (!sRange.IsEmpty())
	{
		int i = 0;
		s_RangeType = sRange.Tokenize(" ", i);
		CString sPartInfo = sRange.Tokenize(" ", i);

		i = 0;
		CString sPortion  = sPartInfo.Tokenize("/", i);
		CString sFullSize  = sPartInfo.Tokenize("/", i);
		i_RangeFullSize = atoi(sFullSize);

		i = 0;
		CString sStartByte  = sPortion.Tokenize("-", i);
		CString sEndByte  = sPortion.Tokenize("-", i);
		i_RangeStart = atoi(sStartByte);
		i_RangeEnd = atoi(sEndByte); 
	}

	return true;
}

const char* PDHTMLResponse::GetStrParam( const char* zKey )
{
	MAP_PARAM_ITR itr = map_Params.find(CString(zKey));
	if (itr == map_Params.end())
		return NULL;

	return itr->second;
}

int PDHTMLResponse::GetIntParam( const char* zKey )
{
	MAP_PARAM_ITR itr = map_Params.find(CString(zKey));
	if (itr == map_Params.end())
		return NULL;

	return atoi(itr->second);
}

bool PDHTMLResponse::IsOK()
{
	return s_Response == "200";
}

bool PDHTMLResponse::IsPartialContent()
{
	return s_Response == "206";
}

int PDHTMLResponse::GetContentLength()
{
	const char* zVal = GetStrParam("Content-Length");
	if (!zVal)
		return -1;

	return atoi(zVal);
}

int PDHTMLResponse::GetContentSizeFromContentRange()
{
//Content-Range: bytes 0-99/773217192
	const char* zVal = GetStrParam("Content-Range");
	if (!zVal)
		return -1;

	char zCntRange[100];
	strcpy_s(zCntRange, 100, zVal);

	int iToken = 0;
	char* zCtx = NULL;
	for (char* zToken = strtok_s(zCntRange, "/", &zCtx); 
		zToken ; zToken = strtok_s( NULL, "/", &zCtx))
	{
		if (iToken == 1)
			return atoi(zToken);
		iToken++;
	}


	return atoi(zVal);
}


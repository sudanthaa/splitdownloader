#include "PDPch.h"

#include <WinInet.h>
#include <Shlwapi.h>

#include "PDConfig.h"
#include "PDItem.h"
#include "PDDownloadThread.h"
#include "PDDownloadUnit.h"
#include "PDApp.h"
#include "PDLogger.h"

#define  STR_BUFF_SIZE  700
#define  ERR_BUFF_SIZE	1000
#define  READ_BUFF_SIZE	20000

//**************************************************************************************************
PDItem::PDItem(void)
{
	p_Response = NULL;
	s_EndFile = "";
	i_Status = ITEM_STATE_STARTED;
	i_TotalProgress = 0;
	i_MergeState = 0;
	e_Scheme = INTERNET_SCHEME_UNKNOWN;
}

//**************************************************************************************************
PDItem::~PDItem(void)
{

}

//**************************************************************************************************
bool PDItem::Init( const char* zURL )
{
	s_URL = zURL;

	char zServer[STR_BUFF_SIZE]; 
	char zFilePath[STR_BUFF_SIZE]; 
	char zFileName[STR_BUFF_SIZE]; 

	URL_COMPONENTS tURL;
	tURL.dwStructSize = sizeof( tURL );
	tURL.dwSchemeLength    = 1;
	tURL.dwHostNameLength  = 1;
	tURL.dwUserNameLength  = 1;
	tURL.dwPasswordLength  = 1;
	tURL.dwUrlPathLength   = 1;
	tURL.dwExtraInfoLength = 1;

	tURL.lpszScheme     = NULL;
	tURL.lpszHostName   = NULL;
	tURL.lpszUserName   = NULL;
	tURL.lpszPassword   = NULL;
	tURL.lpszUrlPath    = NULL;
	tURL.lpszExtraInfo  = NULL;

	BOOL bSucess = InternetCrackUrl(s_URL, strlen(s_URL),0, &tURL);

	if (!bSucess)
		return false;

	strncpy_s(zServer, tURL.lpszHostName,tURL.dwHostNameLength);
	strncpy_s(zFilePath, tURL.lpszUrlPath,tURL.dwUrlPathLength);
	strncpy_s(zFileName, tURL.lpszHostName,tURL.dwHostNameLength);

	TRACE("URL decoded\n");
	TRACE("Host: %s\n", zServer);
	TRACE("Port: %d\n", tURL.nPort);
	TRACE("Path: %s\n", zFilePath);

	s_Server = zServer;
	s_FilePath = zFilePath;
	i_Port = tURL.nPort;
	e_Scheme = tURL.nScheme;

	s_ExtraInfo = tURL.lpszExtraInfo;

	CString sID = s_Server.Left(50);
	sID += ".";
	sID += s_FilePath.Right(50);

	char zID[STR_BUFF_SIZE];
	strcpy_s(zID, STR_BUFF_SIZE, sID);
	int i = 0;
	for (; i <= sID.GetLength(); i++)
	{
		if ((zID[i] <= '9' && zID[i] >='0' )||
			(zID[i] <= 'z' && zID[i] >='a' )||
				(zID[i] <= 'Z' && zID[i] >='A') || zID[i] == '-')
		{
			// do nothing
		}
		else
		{
			zID[i] = '_';
		}
	}
	zID[i] = 0;

	CTime tCur = CTime::GetCurrentTime();


	if (CFG()->b_Recover && CFG()->s_RecoverID.GetLength() > 0)
		s_ID.Format("%s_%s", zID, CFG()->s_RecoverID);
	else
		s_ID.Format("%s_%s", zID, tCur.Format("%Y%m%d%H%M%S"));


	char* zCtx = NULL;
	for (char* zToken = strtok_s(zFilePath, "/", &zCtx); 
			zToken ; zToken = strtok_s( NULL, "/", &zCtx))
	{
		s_EndFile = zToken;
	}

	t_StartTime = CTime::GetCurrentTime();
	return true;
}


//**************************************************************************************************
int PDItem::GetSize()
{
	if (CFG()->b_DefineSize)
		return i_Size;
	else 
		return i_Size;
}

//**************************************************************************************************
void PDItem::AddUnit( PDDownloadUnit* pUnit )
{
	lst_Units.push_back(pUnit);
	lst_IncompleteUnits.push_back(pUnit);
}

//**************************************************************************************************
PDDownloadUnit* PDItem::PopIncomplete()
{
	if (lst_IncompleteUnits.size() <= 0)
		return NULL;

	PDDownloadUnit* pUnit = lst_IncompleteUnits.front();
	lst_IncompleteUnits.pop_front();

	return pUnit;
}

//**************************************************************************************************
bool PDItem::HasIncomplete()
{
	return lst_IncompleteUnits.size() > 0;
}

//**************************************************************************************************
void PDItem::AddCompletedUnit( PDDownloadUnit* pUnit )
{
	lst_CompleteUnits.push_back(pUnit); 
}

//**************************************************************************************************
bool PDItem::IsAllComplete()
{
	return lst_Units.size() == lst_CompleteUnits.size();
}

//**************************************************************************************************
void PDItem::Finalize()
{
	TRACE("Finalizing content\n");
	TRACE("Sleep for sync\n");
	Sleep(1000);

	TRACE("Start merging files\n");

	CString sFile;
	if (s_EndFile.GetLength() > 0)
		sFile = s_EndFile;
	else 
		sFile.Format("%s.final.dat", s_ID);

	int iBlockSize = READ_BUFF_SIZE;
	char* pBuf = new char[iBlockSize];


	CString sDownloadFile;
	sDownloadFile.Format("%s\\%s", PDConfig::p_Inst->s_DownloadsDirectory, sFile);

	CFile oFile;
	CFileException ex;
	if (!oFile.Open(sDownloadFile, CFile::modeWrite | CFile::modeCreate, &ex))
	{
		TCHAR szError[ERR_BUFF_SIZE];
		ex.GetErrorMessage(szError, ERR_BUFF_SIZE);
		TRACE("File open error. [%s], err=%s", sDownloadFile, szError);
		return;
	}

	TRACE("Merging file opened.[%s]\n", sDownloadFile);

	LST_UNIT_ITR itr = lst_Units.begin();
	for (; itr != lst_Units.end(); itr++)
	{
		PDDownloadUnit* pUnit = *itr;
		CFile oUnitFile;
		if (!oUnitFile.Open(pUnit->GetFile(), CFile::modeRead, &ex))
		{
			TCHAR szError[ERR_BUFF_SIZE];
			ex.GetErrorMessage(szError, ERR_BUFF_SIZE);
			PDLog("File open error. [%s], err=%s", pUnit->GetFile(), szError);
			return;
		}

		SetMergingFile(pUnit->GetFile());

		PDLog("Start reading part file. [%s]", pUnit->GetFile());

		int iRead = oUnitFile.Read(pBuf, iBlockSize);
		while (iRead > 0)
		{
			IncrementMergeState(iRead);
			oFile.Write(pBuf, iRead);
			iRead = oUnitFile.Read(pBuf, iBlockSize);
		}

		oUnitFile.Close();
		
	}

	oFile.Flush();
	oFile.Close();

	// Remove files
	//////////////////////////////////////////////////////////////////////////
	Sleep(200); // Give some time for file close.
	itr = lst_Units.begin();

	if (CFG()->b_CleanFiles)
	{
		for (; itr != lst_Units.end(); itr++)
		{
			PDDownloadUnit* pUnit = *itr;
			CFile::Remove(pUnit->GetFile());
		}
	}

	IncrementMergeState(1); // This will close the file merge-dialog

	CString sMsg;
	sMsg.Format("Download is successfully completed and written to .. %s", sDownloadFile);
	AfxMessageBox(sMsg, MB_ICONINFORMATION | MB_OK);

	TRACE(" Merge completed.\n");
	TRACE("=======================================\n");
}

//**************************************************************************************************
LST_UNIT* PDItem::GetUnits()
{
	return &lst_Units;
}

//**************************************************************************************************
void PDItem::SetStatus( int iStatus )
{
	o_Mutex.Lock();
	i_Status = iStatus;
	o_Mutex.Unlock();
}

//**************************************************************************************************
int PDItem::GetStatus()
{
	int iStatus = 0;
	o_Mutex.Lock();
	iStatus = i_Status;
	o_Mutex.Unlock();

	return iStatus;
}

//**************************************************************************************************
bool PDItem::IsSizeCalcError()
{
	return (GetStatus() == ITEM_STATE_SIZE_FIND_ERROR);
}

//**************************************************************************************************
bool PDItem::IsSizeFound()
{
	return (GetStatus() == ITEM_STATE_SIZE_FOUND);
}

//**************************************************************************************************
bool PDItem::IsDownloading()
{
	return (GetStatus() == ITEM_STATE_DOWNLOAD);
}

//**************************************************************************************************
void PDItem::SetSizeCalcError()
{
	SetStatus(ITEM_STATE_SIZE_FIND_ERROR);
}

//**************************************************************************************************
void PDItem::SetDownloading()
{
	SetStatus(ITEM_STATE_DOWNLOAD);
}

//**************************************************************************************************
void PDItem::SetSizeFound()
{
	SetStatus(ITEM_STATE_SIZE_FIND_ERROR);
}


//**************************************************************************************************
void PDItem::IncTotalProgress( int iBytes )
{
	o_TotalProgressMutex.Lock();
	i_TotalProgress += iBytes;
	o_TotalProgressMutex.Unlock();
}

//**************************************************************************************************
int PDItem::GetTotalProgress()
{
	int iProgress = 0;
	o_TotalProgressMutex.Lock();
	iProgress = i_TotalProgress;
	o_TotalProgressMutex.Unlock();

	return iProgress;
}

//**************************************************************************************************
int PDItem::GetElapsed()
{
	CTime tCur = CTime::GetCurrentTime();
	__time64_t ui64Cur = tCur.GetTime();
	__time64_t ui64Init = t_StartTime.GetTime();

	__time64_t ui64Diff = (ui64Cur - ui64Init);
	int iElapsed = (int)ui64Diff; 

	return iElapsed;
}

//**************************************************************************************************
void PDItem::Cancel()
{
	SetStatus(ITEM_STATE_CANCELLED);
}

bool PDItem::IsCancelled()
{
	return (GetStatus() == ITEM_STATE_CANCELLED);
}

//**************************************************************************************************
int PDItem::GetMergeState()
{
	int iMergeState = 0;
	o_MutexMegeState.Lock();
	iMergeState = i_MergeState;
	o_MutexMegeState.Unlock();

	return iMergeState;
}

//**************************************************************************************************
void PDItem::SetMergeState( int iMergeState )
{
	o_MutexMegeState.Lock();
	i_MergeState = iMergeState;
	o_MutexMegeState.Unlock();
}

//**************************************************************************************************
void PDItem::GetMergingFile( CString& sFile )
{
	o_MutexMegeState.Lock();
	sFile = s_MergingFile;
	o_MutexMegeState.Unlock();
}

//**************************************************************************************************
void PDItem::SetMergingFile( const char* zFile )
{
	o_MutexMegeState.Lock();
	s_MergingFile = zFile;
	o_MutexMegeState.Unlock();
}

//**************************************************************************************************
void PDItem::IncrementMergeState( int iValue )
{
	o_MutexMegeState.Lock();
	i_MergeState += iValue;
	o_MutexMegeState.Unlock();
}
//**************************************************************************************************



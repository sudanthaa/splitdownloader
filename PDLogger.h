#pragma once


void	PDLogDebug(const char* zFmt, ...);
void	PDLog(const char* zFmt, ...);

void	RWLogEx(const char* zModule, const char* zFmt, ...);
void	PDMsgBox(const char* zFmt, ...);
void	RWMsgBoxWarn(const char* zFmt, ...);
void	RWMsgBoxInfo(const char* zFmt, ...);
void	RWMsgBoxErr(const char* zFmt, ...);
void	RWTrace(const char* zFmt, ...);

class PDLogger
{
public:
	PDLogger(void);
	~PDLogger(void);

	void	LogString(const char* zLog, const char* zPrefix = "");
	void	LogStringEx(const char* zLog);
	void	PrintHeaderBanner();
	void	LogOSInfo();
	void	LogSwitch();
	void	ShiftName(const char* zPrefix);
	static  BOOL GetOSDisplayString( LPTSTR pszOS);

	static PDLogger o_Logger;
	static bool		b_LogInput;
	static bool		b_LogOutput;

public:

	CFile	o_File;
	bool	b_Initated;

	CMutex	o_WriteLock;
	CMutex	o_Mutex;

protected:
	bool	GetOnlyPath(const char* zFullName, CString& sPath);
	CString GetAppKey();
	void	RenameAndSetFileNameToKey(const char* zFile);
};


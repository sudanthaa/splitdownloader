#include "PDPch.h"
#include "PDDownloadUnit.h"
#include "PDItem.h"
#include "PDConfig.h"

PDDownloadUnit::PDDownloadUnit(PDItem* pItem, int iStart, int iSize)
:p_Item(pItem)
{
	i_State = ITEM_STATE_IDLE;
	i_Size = iSize;
	i_Start = iStart;
	i_Progress = 0;
	s_File.Format("%s\\%s_%d-%d.dat", PDConfig::p_Inst->s_DownloadsCacheDirectory, pItem->s_ID , iStart, iSize);
}

PDDownloadUnit::~PDDownloadUnit(void)
{
}


void PDDownloadUnit::IncProgress( int iProgress )
{
	o_ProgressLock.Lock();
	i_Progress += iProgress;
	o_ProgressLock.Unlock();

	p_Item->IncTotalProgress(iProgress);
}

int PDDownloadUnit::GetProgress()
{
	int iProgress;
	o_ProgressLock.Lock();
	iProgress = i_Progress;
	o_ProgressLock.Unlock();

	return iProgress;
}

void PDDownloadUnit::SetState( int iState )
{
	o_StateLock.Lock();
	i_State = iState;
	o_StateLock.Unlock();
}

int PDDownloadUnit::GetState()
{
	int iState;
	o_StateLock.Lock();
	iState = i_State;
	o_StateLock.Unlock();

	return iState;
}

bool PDDownloadUnit::IsComplete()
{
	return (GetState() == ITEM_STATE_COMPLETE);
}

bool PDDownloadUnit::IsDownloading()
{
	return (GetState() == ITEM_STATE_DOWNLOADING);
}

void PDDownloadUnit::SetComplete()
{
	SetState(ITEM_STATE_COMPLETE);
}

void PDDownloadUnit::SetDownloading()
{
	SetState(ITEM_STATE_DOWNLOADING);
}

PDItem* PDDownloadUnit::GetItem()
{
	return p_Item;
}



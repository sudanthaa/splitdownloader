#pragma once

#include "PDResource.h"
#include "afxcmn.h"
#include "afxwin.h"
// PDFileMergeDialog dialog

class PDItem;

class PDFileMergeDialog : public CDialog
{
public:
	PDFileMergeDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~PDFileMergeDialog();

// Dialog Data
	enum { IDD = IDD_FILE_MERGER_DIALOG };

	void	SetItem(PDItem* pItem)	{ p_Item = pItem; };
	void	OnMergeCheckTimer();
	static UINT  MergeTheadFunc(LPVOID pParam);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	CWinThread*		p_Thread;
	PDItem* p_Item;
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CProgressCtrl o_ProgressFileMerge;
	CStatic o_StaticFileMergeState;
};


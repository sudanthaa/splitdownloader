#include "PDPch.h"
#include "PDLogger.h"

#include <strsafe.h>



#define  LOG_EXTENTION		".log"
#define  LOG_FILEBASENAME	"PartialDownloader" 
#define  LOG_FILE_NAME		LOG_FILEBASENAME LOG_EXTENTION
#define  FILENAME_BUFF_LEN	500
#define	 ERROR_BUFF_LEN		2048
#define  MAX_PID_LEN		7

#define  LOG_MODE_STR			"|   LOG|"
#define  LOGMSG_MODE_STR		"|   MSG|"
#define  LOGINPUT_MODE_STR		"| INPUT|"
#define  LOGOUTPUT_MODE_STR		"|OUTPUT|"
#define  LOGDEBUG_MODE_STR		"| DEBUG|"

#define  LOG_MODE_MB			"|    MB|"
#define  LOG_MODE_MBWRN			"|MB-WRN|"
#define  LOG_MODE_MBERR			"|MB-ERR|"
#define  LOG_MODE_MBIFO			"|MB-IFO|"

#define  RW_VSNPRINTF_SPN(b,s,f)	\
	char b[s];					\
	va_list ap##b;				\
	va_start(ap##b, f);			\
	vsnprintf_s(b, s, _TRUNCATE, f, ap##b);	\
	va_end(ap##b);				\


#define  D_LINE	"================================================================================"
#define  S_LINE	"--------------------------------------------------------------------------------"
#define  PLUS_LINE	"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"


PDLogger PDLogger::o_Logger;

bool	PDLogger::b_LogInput = true;
bool	PDLogger::b_LogOutput = true;

#define  ABSOLUTE_LOG_PATH		"logs"

//**************************************************************************************************
CString GetAppPath()
{
	TCHAR zModule[_MAX_PATH];
	GetModuleFileName(NULL, zModule, _MAX_PATH);

	CString sExeName = zModule;
	int iPos = sExeName.ReverseFind('\\');

	//return sExeName.SubStr(0, iPos);
	return 	sExeName.Mid(0, iPos);
}

//**************************************************************************************************
CString GetLogPath()
{
	CString sPath;
	sPath.Format("%s\\%s", GetAppPath(), ABSOLUTE_LOG_PATH);
	return sPath;
}

//**************************************************************************************************
PDLogger::PDLogger(void)
{
	CreateDirectory(GetLogPath(), NULL);
	CString sAppPath = GetLogPath();
	CString sKey = GetAppKey();

	CString sFileName;
	sFileName.Format("%s\\%s-%s" LOG_EXTENTION, 
		sAppPath, LOG_FILEBASENAME, sKey);

	CFileException ex;
	if (!o_File.Open(sFileName, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &ex))
	{
		char zError[ERROR_BUFF_LEN];
		ex.GetErrorMessage(zError, ERROR_BUFF_LEN);
		ex.Delete();
		TRACE("Failed to inital logger. file=%s, error=%s", LOG_FILE_NAME, zError);
		b_Initated = false;
	}
	else
	{
		b_Initated = true;
		LogStringEx(sKey);
		PrintHeaderBanner();
		LogOSInfo();
	}
}

//**************************************************************************************************
void PDLogger::LogSwitch()
{
	CString sNewFileName;
	CString sCurrentFileName = o_File.GetFileName();

	CString sAppPath = GetLogPath();
	PDLog("Switching the log.. . ");
	
	sNewFileName.Format("%s\\%s", sAppPath, sCurrentFileName);
	PDLog("Current file name. [%s]", sNewFileName);

	CString sNewKey = GetAppKey();
	if (b_Initated)
	{
		PDLog("Closing the current file for switching. new_key=%s", sNewKey);
		o_File.Flush();
		o_File.Close();
		b_Initated = false;
	}

	RenameAndSetFileNameToKey(sNewFileName);
	
	CFileException ex;
	if (!o_File.Open(sNewFileName, CFile::modeWrite | CFile::modeCreate, &ex))
	{
		char zError[ERROR_BUFF_LEN];
		ex.GetErrorMessage(zError, ERROR_BUFF_LEN);
		ex.Delete();
		TRACE("Failed to inital logger. file=%s, error=%s", LOG_FILE_NAME, zError);
		b_Initated = false;
	}
	else
	{
		b_Initated = true;
		LogStringEx(sNewKey);
		PrintHeaderBanner();
		LogOSInfo();

		PDLog("Log switched successfully", sNewFileName);
	}
}

//**************************************************************************************************
void PDLogger::ShiftName( const char* zPrefix )
{
	CString sAppPath = GetLogPath();
	PDLog("Switching the log.. . ");

	CString sCurFile = o_File.GetFileName();
	CString sCurrentFile;
	CString sNewFileName;

	sNewFileName.Format("%s\\" LOG_FILEBASENAME "-%s" LOG_EXTENTION, 
		sAppPath, zPrefix);
	sCurrentFile.Format("%s\\%s", sAppPath, sCurFile);

	RenameAndSetFileNameToKey(sNewFileName);

	PDLog("Current file name. [%s]", sCurrentFile);
	PDLog("New file name. [%s]", sNewFileName);

	o_File.Flush();
	o_File.Close();
	b_Initated = false;

	try
	{
		CFile::Rename(sCurrentFile, sNewFileName);
	}
	catch (CFileException* ex)
	{
		char zError[ERROR_BUFF_LEN];
		ex->GetErrorMessage(zError, ERROR_BUFF_LEN);
		ex->Delete();
		TRACE("Failed rename. src=%s, dest=%s, error=%s", sCurrentFile,
			sNewFileName, zError);
	}

	CFileException ex;
	if (!o_File.Open(sNewFileName, CFile::modeNoTruncate |
				CFile::modeWrite | CFile::modeCreate, &ex))
	{
		char zError[ERROR_BUFF_LEN];
		ex.GetErrorMessage(zError, ERROR_BUFF_LEN);
		ex.Delete();
		TRACE("Failed to inital logger. file=%s, error=%s", sNewFileName, zError);
		b_Initated = false;
	}
	else
	{
		b_Initated = true;
		o_File.SeekToEnd();
		PDLog("Log renamed successfully.");
	}
}

//**************************************************************************************************
PDLogger::~PDLogger(void)
{
	if (b_Initated)
	{
		o_File.Flush();
		o_File.Close();
	}
}

//**************************************************************************************************
void PDLogger::LogString( const char* zLog, const char* zPrefix)
{
	if (!b_Initated)
		return;

	o_Mutex.Lock();

	CTime t = CTime::GetCurrentTime();
	CString sTime = t.Format("%Y%m%d-%H:%M:%S");
	o_File.Write(sTime, sTime.GetLength());
	o_File.Write(zPrefix, strlen(zPrefix));
	
	LogStringEx(zLog);
	o_Mutex.Unlock();
}

//**************************************************************************************************
void PDLogger::LogStringEx( const char* zLog )
{
	if (!b_Initated)
		return;

	o_WriteLock.Lock();
	o_File.Write(zLog, strlen(zLog));
	o_File.Write("\n", 1);
	o_File.Flush();
	o_WriteLock.Unlock();
}


//**************************************************************************************************
void PDLogger::PrintHeaderBanner()
{
	SYSTEM_INFO nsinfo;
	GetNativeSystemInfo(&nsinfo);

	OSVERSIONINFO osinfo;
	GetVersionEx(&osinfo);
	
	DWORD dwLen = 30;
	char zUserName[30];
	
	GetUserName(zUserName, &dwLen);

	PDLog("================================================================================");
	PDLog(" Reporting workstation log                                                     ");
	PDLog("--------------------------------------------------------------------------------");
	PDLog(" System User  : %s", zUserName);
//	PDLog(" Process ID   : %d", _getpid());
	PDLog("================================================================================");
	
//	PDLog("Relman version string..\n"S_LINE"\n%s\n"S_LINE,GetRelManVersionString(RMVT_DEFAULT));
}

//**************************************************************************************************
void PDLog( const char* zFmt, ... )
{
	RW_VSNPRINTF_SPN(zValue, 5000, zFmt)
	PDLogger::o_Logger.LogString(zValue, LOG_MODE_STR);
}

//**************************************************************************************************
void RWLogEx(const char* zModule, const char* zFmt, ... )
{
	RW_VSNPRINTF_SPN(zValue, 5000, zFmt)
	PDLogger::o_Logger.LogString(zValue, LOG_MODE_STR);
}



//**************************************************************************************************
void PDLogDebug( const char* zFmt, ... )
{
	RW_VSNPRINTF_SPN(zValue, 5000, zFmt)
	PDLogger::o_Logger.LogString(zValue, LOGDEBUG_MODE_STR);
}

//**************************************************************************************************
void PDMsgBox(const char* zFmt, ... )
{
	RW_VSNPRINTF_SPN(zValue, 5000, zFmt)
	PDLogger::o_Logger.LogString(zValue, LOG_MODE_MB);
	AfxMessageBox(zValue);
}

//**************************************************************************************************
void RWMsgBoxWarn( const char* zFmt, ... )
{
	RW_VSNPRINTF_SPN(zValue, 5000, zFmt)
	PDLogger::o_Logger.LogString(zValue, LOG_MODE_MBWRN);
	AfxMessageBox(zValue, MB_OK | MB_ICONWARNING);
}

//**************************************************************************************************
void RWMsgBoxInfo( const char* zFmt, ... )
{
	RW_VSNPRINTF_SPN(zValue, 5000, zFmt)
	PDLogger::o_Logger.LogString(zValue, LOG_MODE_MBIFO);
	AfxMessageBox(zValue, MB_OK | MB_ICONINFORMATION);
}

//**************************************************************************************************
void RWMsgBoxErr( const char* zFmt, ... )
{
	RW_VSNPRINTF_SPN(zValue, 5000, zFmt)
	PDLogger::o_Logger.LogString(zValue, LOG_MODE_MBERR);
	AfxMessageBox(zValue, MB_OK | MB_ICONERROR);
}

//**************************************************************************************************
void RWTrace( const char* zFmt, ... )
{
	RW_VSNPRINTF_SPN(zValue, 5000, zFmt)
	//OnTrace(zValue);
}


//**************************************************************************************************
#define RW_OSINFO_BUFSIZE 256
typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
typedef BOOL (WINAPI *PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);

//**************************************************************************************************
void PDLogger::LogOSInfo()
{
	TCHAR szOS[RW_OSINFO_BUFSIZE];
	*szOS = 0;

	if( GetOSDisplayString( szOS ) )
		PDLog("SystemInfo...\n"S_LINE"\n%s\n"S_LINE, szOS);
}

//**************************************************************************************************
BOOL PDLogger::GetOSDisplayString( LPTSTR pszOS)
{
	OSVERSIONINFOEX osvi;
	SYSTEM_INFO si;
	PGNSI pGNSI;
	PGPI pGPI;
	BOOL bOsVersionInfoEx;
	DWORD dwType;

	ZeroMemory(&si, sizeof(SYSTEM_INFO));
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));

	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	bOsVersionInfoEx = GetVersionEx((OSVERSIONINFO*) &osvi);

	if (bOsVersionInfoEx == 0 ) 
		return FALSE;

	// Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.

	pGNSI = (PGNSI) GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "GetNativeSystemInfo");
	
	if (NULL != pGNSI)
		pGNSI(&si);
	else 
		GetSystemInfo(&si);

	if (VER_PLATFORM_WIN32_NT == osvi.dwPlatformId && 
		osvi.dwMajorVersion > 4 )
	{
		StringCchCopy(pszOS, RW_OSINFO_BUFSIZE, TEXT("Microsoft "));

		// Test for the specific product.	

		if ( osvi.dwMajorVersion == 6 )
		{
			if( osvi.dwMinorVersion == 0 )
			{
				if( osvi.wProductType == VER_NT_WORKSTATION )
					StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Windows Vista "));
				else StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Windows Server 2008 " ));
			}

			if ( osvi.dwMinorVersion == 1 )
			{
				if( osvi.wProductType == VER_NT_WORKSTATION )
					StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Windows 7 "));
				else StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Windows Server 2008 R2 " ));
			}

			pGPI = (PGPI) GetProcAddress(
				GetModuleHandle(TEXT("kernel32.dll")), 
				"GetProductInfo");

			pGPI( osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);

			switch( dwType )
			{
			case PRODUCT_ULTIMATE:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Ultimate Edition" ));
				break;
// 			case PRODUCT_PROFESSIONAL:
// 				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Professional" ));
				break;
			case PRODUCT_HOME_PREMIUM:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Home Premium Edition" ));
				break;
			case PRODUCT_HOME_BASIC:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Home Basic Edition" ));
				break;
			case PRODUCT_ENTERPRISE:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Enterprise Edition" ));
				break;
			case PRODUCT_BUSINESS:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Business Edition" ));
				break;
			case PRODUCT_STARTER:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Starter Edition" ));
				break;
			case PRODUCT_CLUSTER_SERVER:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Cluster Server Edition" ));
				break;
			case PRODUCT_DATACENTER_SERVER:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Datacenter Edition" ));
				break;
			case PRODUCT_DATACENTER_SERVER_CORE:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Datacenter Edition (core installation)" ));
				break;
			case PRODUCT_ENTERPRISE_SERVER:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Enterprise Edition" ));
				break;
			case PRODUCT_ENTERPRISE_SERVER_CORE:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Enterprise Edition (core installation)" ));
				break;
			case PRODUCT_ENTERPRISE_SERVER_IA64:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Enterprise Edition for Itanium-based Systems" ));
				break;
			case PRODUCT_SMALLBUSINESS_SERVER:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Small Business Server" ));
				break;
			case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Small Business Server Premium Edition" ));
				break;
			case PRODUCT_STANDARD_SERVER:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Standard Edition" ));
				break;
			case PRODUCT_STANDARD_SERVER_CORE:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Standard Edition (core installation)" ));
				break;
			case PRODUCT_WEB_SERVER:
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Web Server Edition" ));
				break;
			}
		}

		if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
		{
			if( GetSystemMetrics(SM_SERVERR2) )
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Windows Server 2003 R2, "));
			else if ( osvi.wSuiteMask & VER_SUITE_STORAGE_SERVER )
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Windows Storage Server 2003"));
// 			else if ( osvi.wSuiteMask & VER_SUITE_WH_SERVER )
// 				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Windows Home Server"));
			else if( osvi.wProductType == VER_NT_WORKSTATION &&
				si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64)
			{
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Windows XP Professional x64 Edition"));
			}
			else StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Windows Server 2003, "));

			// Test for the server type.
			if ( osvi.wProductType != VER_NT_WORKSTATION )
			{
				if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_IA64 )
				{
					if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
						StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Datacenter Edition for Itanium-based Systems" ));
					else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
						StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Enterprise Edition for Itanium-based Systems" ));
				}

				else if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64 )
				{
					if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
						StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Datacenter x64 Edition" ));
					else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
						StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Enterprise x64 Edition" ));
					else StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Standard x64 Edition" ));
				}

				else
				{
					if ( osvi.wSuiteMask & VER_SUITE_COMPUTE_SERVER )
						StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Compute Cluster Edition" ));
					else if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
						StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Datacenter Edition" ));
					else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
						StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Enterprise Edition" ));
					else if ( osvi.wSuiteMask & VER_SUITE_BLADE )
						StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Web Edition" ));
					else StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Standard Edition" ));
				}
			}
		}

		if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
		{
			StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Windows XP "));
			if( osvi.wSuiteMask & VER_SUITE_PERSONAL )
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Home Edition" ));
			else StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Professional" ));
		}

		if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
		{
			StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT("Windows 2000 "));

			if ( osvi.wProductType == VER_NT_WORKSTATION )
			{
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Professional" ));
			}
			else 
			{
				if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
					StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Datacenter Server" ));
				else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
					StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Advanced Server" ));
				else StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( "Server" ));
			}
		}

		// Include service pack (if any) and build number.

		if( _tcslen(osvi.szCSDVersion) > 0 )
		{
			StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT(" ") );
			StringCchCat(pszOS, RW_OSINFO_BUFSIZE, osvi.szCSDVersion);
		}

		TCHAR buf[80];

		StringCchPrintf( buf, 80, TEXT(" (build %d)"), osvi.dwBuildNumber);
		StringCchCat(pszOS, RW_OSINFO_BUFSIZE, buf);

		if ( osvi.dwMajorVersion >= 6 )
		{
			if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64 )
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT( ", 64-bit" ));
			else if (si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_INTEL )
				StringCchCat(pszOS, RW_OSINFO_BUFSIZE, TEXT(", 32-bit"));
		}

		return TRUE; 
	}

	else
	{  
		printf( "This sample does not support this version of Windows.\n");
		return FALSE;
	}
}
//**************************************************************************************************
bool PDLogger::GetOnlyPath( const char* zFullName, CString& sPath )
{
	int iLen = strlen(zFullName);
	for (int i = iLen; i > -1; i--)
	{
		char c = zFullName[i];
		if (c == '\\')
		{
			char zBuff[FILENAME_BUFF_LEN];
			strcpy_s(zBuff, FILENAME_BUFF_LEN, zFullName);
			zBuff[i] = 0;
			sPath = zBuff;
			return true;
		}
	}
	return false;
}

//**************************************************************************************************
CString PDLogger::GetAppKey()
{
	CString sKey;
	CTime t = CTime::GetCurrentTime();
	CString sTime = t.Format("%Y%m%d%H%M%S");

	sKey.Format("%s-%07d", sTime, 1);
	return sKey;
}

//**************************************************************************************************
void PDLogger::RenameAndSetFileNameToKey( const char* zFile )
{
	CString sNewFileName = zFile;
	// if old log exists take key from it.
	//////////////////////////////////////////////////////////////////////////
	CFileStatus fs;
	BOOL bExist = CFile::GetStatus(sNewFileName, fs);
	if (bExist)
	{
		CStdioFile oOldLogFile;
		if (oOldLogFile.Open(sNewFileName, CFile::modeRead))
		{
			// Reading the key in the old file
			CString sOldKey = "";
			oOldLogFile.ReadString(sOldKey);
			oOldLogFile.Close();

			// Generate files new name.
			CString sPath;
			GetOnlyPath(fs.m_szFullName, sPath);
			CString sFullRenamedPath;
			sFullRenamedPath.Format("%s\\" LOG_FILEBASENAME "-%s" LOG_EXTENTION, 
				sPath, sOldKey);

			try
			{
				CFile::Rename(fs.m_szFullName, sFullRenamedPath);
			}
			catch (CFileException* ex)
			{
				char  zError[ERROR_BUFF_LEN];
				ex->GetErrorMessage(zError, ERROR_BUFF_LEN);
				ex->Delete();
			}

		}
		else
		{
			return;
		}
	}
}

//**************************************************************************************************



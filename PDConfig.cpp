#include "PDPch.h"
#include "PDConfig.h"

PDConfig* PDConfig::p_Inst;
PDConfig oInst;

//**************************************************************************************************
PDConfig::PDConfig(void)
{
	p_Inst = this;
	b_DefineSize = false;
	b_CleanFiles = true;
	b_Recover = false;

	i_FetchBlockSize = 1024;
	i_Threads = 3;
	i_DownloadUnitSize = 4 * 1024 * 1024; //4 MB
	b_UseProxy = true;
	s_ProxyServer = "test.jira.com";
	s_DownloadsDirectory = "";
	s_DownloadsCacheDirectory = "";
	i_ProxyPort = 3128;
	i_ProxyMode = PROXY_MODE_SYSTEM;

	i_FileSize = 0;
	i_RetryCount = 3;
	i_AgentType = AGENT_NONE;
	s_URL = "";
	s_RecoverID = "";

	if (s_DownloadsDirectory.IsEmpty())
	{
		TCHAR zUserAppPath[MAX_PATH];
		SHGetSpecialFolderPath(0, zUserAppPath, CSIDL_APPDATA, FALSE);
		CString sDownloadDir;
		sDownloadDir.Format("%s\\PartialDownloader", zUserAppPath);

		CreateDirectory(sDownloadDir, NULL);
		s_DownloadsDirectory = sDownloadDir;
	}

	if (s_DownloadsCacheDirectory.IsEmpty())
	{
		TCHAR zUserAppPath[MAX_PATH];
		SHGetSpecialFolderPath(0, zUserAppPath, CSIDL_APPDATA, FALSE);
		CString sDownloadCacheDir;
		sDownloadCacheDir.Format("%s\\PartialDownloader\\cache", zUserAppPath);

		CreateDirectory(sDownloadCacheDir, NULL);
		s_DownloadsCacheDirectory = sDownloadCacheDir;
	}
}

//**************************************************************************************************
PDConfig::~PDConfig(void)
{
}

//**************************************************************************************************
void PDConfig::SetToRegistry()
{
	CWinApp* pApp = AfxGetApp();

	pApp->WriteProfileString("Config", "Proxy", s_ProxyServer);
	pApp->WriteProfileString("Config", "URL", s_URL);
	pApp->WriteProfileString("Config", "RecoverID", s_RecoverID);
	pApp->WriteProfileString("Config", "DownloadsDir", s_DownloadsDirectory);
	pApp->WriteProfileString("Config", "DownloadsCacheDir", s_DownloadsCacheDirectory);

	pApp->WriteProfileInt("Config", "FetchBlockSize", i_FetchBlockSize);
	pApp->WriteProfileInt("Config", "DownloadUnitSize", i_DownloadUnitSize);
	pApp->WriteProfileInt("Config", "Threads", i_Threads);
	pApp->WriteProfileInt("Config", "ProxyPort", i_ProxyPort);
	pApp->WriteProfileInt("Config", "AgentType", i_AgentType);

	// Writing bools
	pApp->WriteProfileInt("Config", "CleanFiles", b_CleanFiles ? 1:0);
	pApp->WriteProfileInt("Config", "UseProxy", b_UseProxy ? 1:0);
	pApp->WriteProfileInt("Config", "DefineSize", b_DefineSize ? 1:0);
}

//**************************************************************************************************
void PDConfig::LoadFromRegistry()
{
	CWinApp* pApp = AfxGetApp();

	s_ProxyServer = pApp->GetProfileString("Config", "Proxy", s_ProxyServer);
	s_URL = pApp->GetProfileString("Config", "URL", s_URL);
	s_RecoverID = pApp->GetProfileString("Config", "RecoverID", s_RecoverID);
	s_DownloadsDirectory = pApp->GetProfileString("Config", "DownloadsDir", s_DownloadsDirectory);
	s_DownloadsCacheDirectory = pApp->GetProfileString("Config", "DownloadsCacheDir", s_DownloadsCacheDirectory);

	i_FetchBlockSize = pApp->GetProfileInt("Config", "FetchBlockSize", i_FetchBlockSize);
	i_DownloadUnitSize = pApp->GetProfileInt("Config", "DownloadUnitSize", i_DownloadUnitSize);
	i_Threads = pApp->GetProfileInt("Config", "Threads", i_Threads);
	i_ProxyPort = pApp->GetProfileInt("Config", "ProxyPort", i_ProxyPort);
	i_AgentType = pApp->GetProfileInt("Config", "AgentType", i_AgentType);

	// Loading tools
	b_UseProxy = (pApp->GetProfileInt("Config", "UseProxy", b_UseProxy ? 1:0) == 1);
	b_DefineSize = (pApp->GetProfileInt("Config", "DefineSize", b_DefineSize ? 1:0) == 1);
	b_CleanFiles = (pApp->GetProfileInt("Config", "CleanFiles",  b_CleanFiles ? 1:0) == 1);
}

//**************************************************************************************************


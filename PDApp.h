// PartialDownloader.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "PDResource.h"		// main symbols


// CPartialDownloaderApp:
// See PartialDownloader.cpp for the implementation of this class
//

class PDApp : public CWinApp
{
public:
	PDApp();

// Overrides
	public:
	virtual BOOL InitInstance();

	bool	MValid();
	void	MShow(BYTE* pShow);
	bool	MCheck(BYTE* bBuff);

// Implementation

	DECLARE_MESSAGE_MAP()
};


class PDParams : public CCommandLineInfo
{
public:
	PDParams();
	virtual ~PDParams();

	CString		s_RecoverLink;
	bool		b_KeepFiles;

	static PDParams*	p_Inst;

protected:
	void	ParseParam(const TCHAR* pszParam, BOOL bFlag, BOOL bLast);

	bool	b_ValFetch;

	CString		s_LastParam;
};

extern PDApp theApp;



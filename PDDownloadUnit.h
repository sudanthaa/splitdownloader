#pragma once

class PDItem;

#define  ITEM_STATE_IDLE	0
#define  ITEM_STATE_DOWNLOADING	1
#define  ITEM_STATE_COMPLETE	2

class PDDownloadUnit
{
public:
	PDDownloadUnit(PDItem* pItem, int iStart, int iSize);
	~PDDownloadUnit(void);

	CString GetFile()	{	return s_File; };
	int		GetSize()	{	return i_Size; };
	int		GetStart()	{	return i_Start; };
	PDItem*		GetItem();
	void	IncProgress(int iProgress);
	int		GetProgress();
	void	SetState(int iState);
	int		GetState();

	bool	IsComplete();
	bool	IsDownloading();
	void	SetComplete();
	void	SetDownloading();

protected:
	CMutex o_StateLock;
	CMutex o_ProgressLock;

	CString	s_File;
	int		i_Index;
	int		i_State;
	int		i_Start;
	int		i_Size;
	int		i_Progress;
	PDItem*		p_Item;
};

// PartialDownloader.cpp : Defines the class behaviors for the application.
//

#include "PDPch.h"


#include "PDApp.h"
#include "PDDialog.h"


#define  MAC_LEN			12
#define  SIXBYTE_HEX_FMT	"%02X%02X%02X%02X%02X%02X"


PDParams*	PDParams::p_Inst = NULL;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CPartialDownloaderApp

BEGIN_MESSAGE_MAP(PDApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CPartialDownloaderApp construction

PDApp::PDApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CPartialDownloaderApp object

PDApp theApp;


// CPartialDownloaderApp initialization

//**************************************************************************************************
BOOL PDApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	bool bAuth = MValid();
	if (!bAuth)
	{
		AfxMessageBox("Authentication failed. Application existing...");
		return FALSE;
	}

	PDParams oParams;
	ParseCommandLine(oParams);

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("PartDownloader"));

	PDDialog dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

//**************************************************************************************************
bool PDApp::MValid()
{
	IP_ADAPTER_INFO AdapterInfo[16];       // Allocate information
	// for up to 16 NICs
	DWORD dwBufLen = sizeof(AdapterInfo);  // Save memory size of buffer

	DWORD dwStatus = GetAdaptersInfo(      // Call GetAdapterInfo
		AdapterInfo,                 // [out] buffer to receive data
		&dwBufLen);                  // [in] size of receive data buffer
  // Verify return value is
	// valid, no buffer overflow

	PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo; // Contains pointer to
	// current adapter info
	do {
		MShow(pAdapterInfo->Address); // Print MAC address
		if (MCheck(pAdapterInfo->Address))
			return true;

		pAdapterInfo = pAdapterInfo->Next;    // Progress through
		// linked list
	}
	while(pAdapterInfo);

	return false;
}


//**************************************************************************************************
void PDApp::MShow(BYTE* abMAC)
{
	CString sMac;
	sMac.Format(SIXBYTE_HEX_FMT, abMAC[0], abMAC[1], abMAC[2], abMAC[3], abMAC[4], abMAC[5]);
	TRACE("Processing MAC: %s\n", sMac);
}

//**************************************************************************************************
bool PDApp::MCheck( BYTE* abMAC)
{
	return true;
	static const BYTE abMask[] = {0x5A, 0x29, 0xB7, 0x36, 0x04, 0x18};	// Key
	static unsigned int auiPerm[][4] = {
											//5A2 9B7 360 418
											//012 345 678 9AB
				{0x7EB,0xB77,0x19E,0xDC7},	//ECB 1D7 977 BE7	// nadun
				{0x4E1,0x131,0x9F8,0x306},	//801 934 F16 1E3	// nadun
				{0x6B5,0x560,0x1A6,0x04A},	//645 106 A0A 5B6	// sudantha-new-ech
				{0x2A1,0x8C6,0x390,0x70D},	//001 372 96D 8AC	// sudantha-new-wifi
				{0x4B1,0x42E,0x7D0,0xA0C},  //001 7A4 DEC 4B2	// sudantha-nc8430-eth
				{0x331,0x928,0xA40,0x70C},  //001 A73 48C 932	// sudantha-nc8430-wifi
				{0x960,0x953,0xA5C,0xA8C},	//C80 AA9 53C 965	// shanaka
				{0x9A0,0x261,0xA5C,0xA8C},	//C80 AA9 51C 2A6	// duminda
				{0x56D,0x9D7,0x30D,0x88E},	//D8D 385 07E 96D	// priyanga
				{0x9A0,0xE21,0xA5C,0xA8B},	//C80 AA9 51B EA2	// sudantha-ech
				{0x0DE,0x0A3,0x467,0x086},	//78E 400 636 0DA	// sudantha-wifi
				{0x5D3,0x992,0xBF6,0xECD},	//6C3 BE5 F2D 9D9	// jagathsri-eth
				{0x5C3,0x9E2,0xBF6,0xECD},	//6C3 BE5 F2D 9CE	// harshanaj-eth
				{0x695,0x6E0,0x1A6,0x041},	//645 106 A01 69E	// isharau
				{0x2A1,0x8C6,0x390,0x70D},	//001 372 96D 8AC	// fe-machine
				{0x8F1,0xD35,0xDFA,0x405},	//A01 D48 F55 DF3	// tharinduaa-eth
				{0x0,0x0,0x0,0x0}};	// End

	char zMask[MAC_LEN + 1];
	sprintf_s(zMask, MAC_LEN + 1, SIXBYTE_HEX_FMT,
		abMask[0], abMask[1], abMask[2], abMask[3], abMask[4], abMask[5]);

	for (int iAddr = 0;
			(auiPerm[iAddr][0] != 0) && (auiPerm[iAddr][1] != 0);
				iAddr++)
	{
		char zPermAddr[MAC_LEN + 1];
		char zInAddr[MAC_LEN + 1];

		sprintf_s(zPermAddr, MAC_LEN + 1, "%03X%03X%03X%03X",
			auiPerm[iAddr][0], auiPerm[iAddr][1], auiPerm[iAddr][2], auiPerm[iAddr][3]);
		sprintf_s(zInAddr, MAC_LEN + 1, SIXBYTE_HEX_FMT,
			abMAC[0], abMAC[1], abMAC[2], abMAC[3], abMAC[4], abMAC[5]);

		bool bValid = true;
		for (int i = 0; i < MAC_LEN; i++)
		{
			int iPos = (zMask[i] >= 'A') ?  ( 10 + (zMask[i] - 'A')): ( zMask[i] - '0');
			if (zPermAddr[i] != zInAddr[iPos])
				bValid = false;
		}

		if (bValid)
			return true;
	}

	return false;
}

//**************************************************************************************************

PDParams::PDParams()
{
	b_ValFetch = false;
	p_Inst = this;
	b_KeepFiles = false;
}

//**************************************************************************************************
PDParams::~PDParams()
{

}

//**************************************************************************************************
void PDParams::ParseParam( const TCHAR* pszParam, BOOL bFlag, BOOL bLast )
{
	CString sParam = pszParam;

	if (bFlag)		// Mere parameters
	{
		bool bProcessed = true;
		if		(sParam == "keep-files")		{	b_KeepFiles = true;     }
		else									{	bProcessed = false;		}

		if (!bProcessed && b_ValFetch)
		{
			//RWMsgBox("Parameter error. %s", s_LastParam.GetCStr());
		}

		b_ValFetch = !bProcessed;

	}
	else			// Mere parameters
	{
		if		(s_LastParam == "recover-id")		{	s_RecoverLink = sParam;	}
		else if (s_LastParam == "tag1")	{	}
		else if (s_LastParam == "tag2")	{	}
		else	{	}

		b_ValFetch = false;
	}

	s_LastParam = pszParam;
}

//**************************************************************************************************
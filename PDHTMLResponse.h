#pragma once

#include <map>

typedef std::map<CString, CString> MAP_PARAM;
typedef MAP_PARAM::iterator		MAP_PARAM_ITR;

class PDHTMLResponse
{
public:
	PDHTMLResponse(void);
	~PDHTMLResponse(void);

	bool	Init(const char* zStr, int iSize);
	const char* GetStrParam(const char* zKey);
	int		GetIntParam(const char* zKey);
	bool	IsOK();
	bool	IsPartialContent();
	int		GetContentLength();
	int		GetContentSizeFromContentRange();
	int		GetFullSize()	{	return i_RangeFullSize; };
	int		GetDeficitSize()	{	return i_DeficitSize; };
	char*		GetDeficitBuffer()	{	return pz_DeficitBuff; };

protected:
	CString	s_Version;
	CString	s_ResponseReason;
	CString	s_Response;

	char*	pz_DeficitBuff;
	int		i_DeficitSize;

	int		i_HeaderSize;
	MAP_PARAM	map_Params;


	CString s_RangeType;
	int		i_RangeStart;
	int		i_RangeEnd;
	int		i_RangeFullSize;
};

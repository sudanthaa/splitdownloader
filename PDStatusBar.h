#pragma once

class PDDialog;

// PDStatusBar

class PDStatusBar : public CWnd
{
	DECLARE_DYNAMIC(PDStatusBar)

public:
	PDStatusBar(PDDialog* pDlg, CRect rRect);
	virtual ~PDStatusBar();

	PDDialog* p_Dlg;

	void	SetMsg(const char* zMsg);
	void	SetETA(int iSeconds);
	void	SetSpeed(int iBytesSec);
	void	SetElapsed(int iSeconds);

	CString	s_Msg;
	int		i_ETASeconds;
	int		i_Elapsed;
	double	i_DownSpeed;

	CRect	r_Msg;
	CRect	r_ETA;
	CRect	r_Elapsed;
	CRect	r_Speed;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};



#pragma once

#include "PDProgressBar.h"

class PDDownloadUnit;
class PDItem;
class PDDialog;

class PDDownloadThread
{
public:
	PDDownloadThread(int iIndex, PDDialog* pDialog);
	~PDDownloadThread(void);

	bool	Download(PDDownloadUnit* pUnit);
	bool	IsCancelled();
	void	SetUnit(PDDownloadUnit* pUnit);
	void	Execute();
	PDDownloadUnit* GetUnit();
	void	Refresh();
	void	Clean();

protected:

	bool	_SocketDownload(PDDownloadUnit* pUnit);
	bool	_WinInetDownload(PDDownloadUnit* pUnit);
	void	Lock();
	void	Unlock();

	static UINT  TheadFunc(LPVOID pParam);

	int		i_Index;
	PDDialog* p_Dialog;
	PDDownloadUnit* p_Unit;

	CWinThread* p_Thread;
	CMutex	o_Lock;
	PDProgressBar o_ProgressBar;
};

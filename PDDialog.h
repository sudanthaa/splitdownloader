// PartialDownloaderDlg.h : header file
//

#pragma once

#include <list>
#include <vector>

#include "PDResource.h"
#include "afxwin.h"
#include <Ws2bth.h>
#include <BluetoothAPIs.h>

class PDItem;
class PDDownloadThread;
class PDTileView;
class PDStatusBar;

typedef 	std::list<int>  LST_INT;
typedef 	LST_INT::iterator LST_INT_IT;
typedef 	LST_INT::reverse_iterator LST_INT_RIT;

// PDDialog dialog
class PDDialog : public CDialog
{
// Construction
public:
	PDDialog(CWnd* pParent = NULL);	// standard constructor
	~PDDialog();
	void	GetProgressArea(CRect& rRect, int iIndex);
	PDItem*	GetItem()	{	return p_Item; };

// Dialog Data
	enum { IDD = IDD_PARTIALDOWNLOADER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	class BtDevice
	{
	public:
		CString s_Name;
		BTH_ADDR addr_Device;
	};

	void	ScheduleItem(PDItem* pItem);
	void	OnProcessTimer();
	void	UpdateStat();
	bool	GetFileSizeFromSmallSizePartialRequest(PDItem* pItem);
	void	EnumerateDevices();
	void	EnumerateDeviceEx(std::vector<BtDevice*>& aList);
	void	ConvertToWebHashString(const char* zString, char * zOutBuff,int iOutBufLen);
	void	UpdateFieldsFromConfig();
	void	Merge();
	
// Implementation
protected:
	bool 	SetThreadCount(int iThreads);
	void	InitTileView();
	void	InitStatusBar();
	void	JoinAndDeleteThreads();

	HICON	m_hIcon;
	PDItem*	p_Item;

	int		i_TheardCount;
	PDDownloadThread** a_Threads;
	PDTileView*		p_TileView ;
	PDStatusBar*	p_StatusBar;

	std::list<int>	lst_ProgressAtSec;
	std::vector<BtDevice*> a_Devices;

	// Generated message map functions
	virtual BOOL OnInitDialog();

	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDeltaposSpinThreads(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEditProxyServer();
	afx_msg void OnEnChangeEditProxyPort();
	afx_msg void OnBnClickedCheckUseProxy();
	afx_msg void OnEnChangeURL();
	afx_msg void OnEnChangeEditBlockSize();
	afx_msg void OnClose();
	afx_msg void OnEnChangeEditRecoverId();
	afx_msg void OnBnClickedCheckRecover();
	afx_msg void OnBnClickedCheckCleanFiles();
	afx_msg void OnBnClickedButtonDownloadsDir();

	CStatic o_StaticBlockSizeMB;
	CButton o_CheckCleanFiles;

	CEdit o_EditProxyServer;
	CEdit o_EditProxyPort;
	CEdit o_EditThreadCount;
	CEdit o_EditURL;
	CEdit o_EditBlockSize;
	CButton o_CheckRecover;
	CEdit o_EditRecoverID;
};

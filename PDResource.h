//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PartialDownloader.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PARTIALDOWNLOADER_DIALOG    102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_FILE_MERGER_DIALOG          129
#define IDC_BUTTON_TEST                 1000
#define IDC_EDIT_URL                    1001
#define IDC_STATIC_URL                  1003
#define IDC_CHECK_USE_PROXY             1005
#define IDC_CHECK_DEFINE_LENGTH         1006
#define IDC_EDIT_FILE_SIZE              1007
#define IDC_EDIT_PROXY_SERVER           1008
#define IDC_EDIT_PROXY_PORT             1009
#define IDC_STATIC_PROXY_PORT           1010
#define IDC_STATIC_FRAME_PROXY_SETTINGS 1011
#define IDC_STATIC_PROXY_SERVER         1012
#define IDC_STATIC_PROGRESS_AREA        1014
#define IDC_EDIT_THREADS                1015
#define IDC_SPIN_THREADS                1016
#define IDC_STATIC_THREADS              1017
#define IDC_STATIC_SIZE                 1018
#define IDC_STATIC_TILE_VIEW_AREA       1019
#define IDC_STATIC_AUTHOR               1019
#define IDC_STATIC_BLOCK_SIZE           1020
#define IDC_EDIT_BLOCK_SIZE             1021
#define IDC_STATIC_STATUS_AREA          1022
#define IDC_PROGRESS_MERGE              1023
#define IDC_EDIT_RECOVER_ID             1023
#define IDC_STATIC_FILE_MREGE_STATE     1024
#define IDC_STATIC_RECOVER_ID           1024
#define IDC_CHECK_CLEAN_FILES           1025
#define IDC_STATIC_BLOCK                1026
#define IDC_STATIC_BLOCK_SIZE_MB        1027
#define IDC_STATIC_RECOVER              1028
#define IDC_CHECK_RECOVER               1029
#define IDC_STATIC_REQUEST_AGENT        1030
#define IDC_BUTTON_DOWNLOADS_DIR        1037
#define IDC_RADIO_NO_PROXY              1038
#define IDC_RADIO_NO_SYSTEM_PROXY       1039
#define IDC_RADIO_NO_SYSTEM_PROXY2      1040
#define IDC_RADIO_NO_CUSTOM_PROXY       1040

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1039
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

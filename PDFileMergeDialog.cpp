// PDFileMergeDialog.cpp : implementation file
//

#include "PDPch.h"
#include "PDFileMergeDialog.h"
#include "PDItem.h"


// PDFileMergeDialog dialog

#define  ID_TIMER_MERGE_CHECK		11001
#define  MERGE_PROGRESS_MAX			1000


PDFileMergeDialog::PDFileMergeDialog(CWnd* pParent /*=NULL*/)
	: CDialog(PDFileMergeDialog::IDD, pParent)
{
	p_Item = NULL;
	p_Thread = NULL;
}

PDFileMergeDialog::~PDFileMergeDialog()
{
}

void PDFileMergeDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_MERGE, o_ProgressFileMerge);
	DDX_Control(pDX, IDC_STATIC_FILE_MREGE_STATE, o_StaticFileMergeState);
}


BEGIN_MESSAGE_MAP(PDFileMergeDialog, CDialog)
	ON_WM_TIMER()
END_MESSAGE_MAP()


UINT PDFileMergeDialog::MergeTheadFunc( LPVOID pParam )
{
	PDItem* pItem = (PDItem*)pParam;
	pItem->Finalize();

	return 0;
}

// PDFileMergeDialog message handlers

BOOL PDFileMergeDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here

	if (!p_Item)
		return FALSE;

	o_ProgressFileMerge.SetRange(0, MERGE_PROGRESS_MAX);

	p_Thread = AfxBeginThread(MergeTheadFunc, (LPVOID)p_Item, 0, CREATE_SUSPENDED);
	SetTimer(ID_TIMER_MERGE_CHECK, 500, NULL);
	p_Thread->ResumeThread();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void PDFileMergeDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	// TODO: Add your message handler code here and/or call default
	if (nIDEvent = ID_TIMER_MERGE_CHECK)
		OnMergeCheckTimer();

	CDialog::OnTimer(nIDEvent);
}

void PDFileMergeDialog::OnMergeCheckTimer()
{
	int iMergeState = p_Item->GetMergeState();

	__int64 i64State = iMergeState;
	i64State *= MERGE_PROGRESS_MAX;
	i64State /= p_Item->GetSize();

	int iProgressPos = (int )i64State;
	o_ProgressFileMerge.SetPos(iProgressPos);
	
	CString sMergingFile;
	p_Item->GetMergingFile(sMergingFile);


	CString sMsg;
	sMsg.Format("(%d of %d) Merging... [%s]", iMergeState , p_Item->GetSize(), sMergingFile);
	o_StaticFileMergeState.SetWindowText(sMsg);

	if (iMergeState > p_Item->GetSize())  // > is used rather than >= since to cover the over-head also in merging
	{
		KillTimer(ID_TIMER_MERGE_CHECK);
		EndDialog(IDOK);
	}
}
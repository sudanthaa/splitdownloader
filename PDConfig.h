#pragma once

#define  CFG() PDConfig::p_Inst

#define  AGENT_WEB_AGENT  0
#define  AGENT_TCPIP  1
#define  AGENT_BLUETOOTH  2
#define  AGENT_NONE		4

#define  PROXY_MODE_NONE	0
#define  PROXY_MODE_SYSTEM	1
#define  PROXY_MODE_CUSTOM	2

class PDConfig
{
public:
	PDConfig(void);
	~PDConfig(void);

	void	SetToRegistry();
	void	LoadFromRegistry();

	static PDConfig* p_Inst;

	int		i_FetchBlockSize;
	int		i_DownloadUnitSize;
	int		i_Threads;
	int		i_RetryCount;
	int		i_AgentType;

	bool	b_DefineSize;
	bool	b_CleanFiles;

	bool	b_Recover;
	bool	b_UseProxy;
	int		i_FileSize;
	int		i_ProxyPort;
	int		i_ProxyMode;

	CString		s_RecoverID;
	CString		s_URL;
	CString		s_ProxyServer;

	CString		s_DownloadsDirectory;
	CString		s_DownloadsCacheDirectory;
};

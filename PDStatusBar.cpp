// PDStatusBar.cpp : implementation file
//

#include "PDPch.h"
#include "PDStatusBar.h"
#include "PDDialog.h"

// PDStatusBar

#define  BACK_COLOR RGB(230,230,230)

IMPLEMENT_DYNAMIC(PDStatusBar, CWnd)

PDStatusBar::PDStatusBar(PDDialog* pDlg, CRect rRect)
{
	p_Dlg = pDlg;
	Create(NULL, NULL, WS_CHILD|WS_VISIBLE|WS_BORDER, rRect, pDlg, 10013);

	s_Msg = "Idle...";
	i_ETASeconds = 0;
	i_DownSpeed = 0;
	i_Elapsed = 0;

	r_ETA = rRect;
	r_Msg = rRect;
	r_ETA.left = r_ETA.right - 85;
	r_Elapsed.right = r_ETA.left - 1;
	r_Elapsed.left = r_Elapsed.right - 105;
	r_Speed.right = r_Elapsed.left -1;
	r_Speed.left = r_Speed.right - 110;
	r_Msg.right = r_Speed.left - 1;

	r_Msg.left += 2;
}

PDStatusBar::~PDStatusBar()
{
}


BEGIN_MESSAGE_MAP(PDStatusBar, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void PDStatusBar::OnPaint()
{
	CPaintDC dc(this);

	CRect rRect;
	GetClientRect(rRect);
	dc.FillSolidRect(rRect, BACK_COLOR);

	CFont* pOldFont = dc.SelectObject(p_Dlg->GetFont());

	// Draw Msg
	dc.FillSolidRect(r_Msg, BACK_COLOR);
	dc.TextOut(r_Msg.left,1, s_Msg);

	// Draw ETA
	CString sETA;
	int iSec = i_ETASeconds % 60;
	int iInter =  i_ETASeconds / 60;
	int iMin = iInter % 60;
	int iHr =  iInter / 60;
	sETA.Format("ETA: %02d:%02d:%02d", iHr, iMin, iSec);
	dc.TextOut(r_ETA.left,1, sETA);


	CString sSpeed;
	double dSpeed = i_DownSpeed;
	double dKBSpeed = dSpeed / 1024.0;
	if (dKBSpeed > 1000.0)
		sSpeed.Format("Speed: %.2f MB/s", dKBSpeed / 1024.0);
	else
		sSpeed.Format("Speed: %.2f KB/s", dKBSpeed);
	dc.TextOut(r_Speed.left, 1, sSpeed);


	dc.FillSolidRect(r_Elapsed, BACK_COLOR);
	CString sElapsed;
	int iSecE = i_Elapsed % 60;
	int iInterE =  i_Elapsed / 60;
	int iMinE = iInterE % 60;
	int iHrE =  iInterE / 60;

	sElapsed.Format("Elapsed: %02d:%02d:%02d", iHrE, iMinE, iSecE);
	dc.TextOut(r_Elapsed.left, 1, sElapsed);


	dc.SelectObject(pOldFont);
}


void PDStatusBar::SetMsg( const char* zMsg )
{
	s_Msg = zMsg;
}

void PDStatusBar::SetETA( int iSeconds )
{
	i_ETASeconds = iSeconds;
}

void PDStatusBar::SetSpeed( int iBytesSec )
{
	i_DownSpeed = iBytesSec;
}


void PDStatusBar::SetElapsed( int iSeconds )
{
	i_Elapsed = iSeconds;
}
// PDStatusBar message handlers



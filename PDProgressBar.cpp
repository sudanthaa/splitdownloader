// PDProgressBar.cpp : implementation file
//

#include "PDPch.h"
#include "PDProgressBar.h"


// PDProgressBar

IMPLEMENT_DYNAMIC(PDProgressBar, CProgressCtrl)

PDProgressBar::PDProgressBar()
{
	o_BackBrush.CreateSolidBrush(RGB(255,200,200)); 
}

PDProgressBar::~PDProgressBar()
{
}


BEGIN_MESSAGE_MAP(PDProgressBar, CProgressCtrl)
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()



// PDProgressBar message handlers



HBRUSH PDProgressBar::CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/)
{
	// TODO:  Change any attributes of the DC here

	// TODO:  Return a non-NULL brush if the parent's handler should not be called
	return o_BackBrush;
}

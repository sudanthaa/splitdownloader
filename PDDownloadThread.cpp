#include "PDPch.h"

#include <WinInet.h>
#include <Shlwapi.h>
#include <io.h>

#include "PDItem.h"
#include "PDDownloadUnit.h"
#include "PDDownloadThread.h"
#include "PDConfig.h"
#include "PDDialog.h"
#include "PDLogger.h"

#define  HEADER_BUF_SIZE			5000
#define  DOWNLOAD_PROGRESS_RANGE	100
#define  FILE_EXISTS(f)			(_access (f, 0) == 0)

//**************************************************************************************************
PDDownloadThread::PDDownloadThread(int iIndex, PDDialog* pDialog)
:i_Index(iIndex)
,p_Dialog(pDialog)
{
	p_Unit = NULL;
	p_Thread = NULL;
	CRect rClient;
	pDialog->GetProgressArea(rClient, iIndex);
	o_ProgressBar.Create(WS_VISIBLE | WS_CHILD, rClient, pDialog, 1200 + iIndex);
}

//**************************************************************************************************
PDDownloadThread::~PDDownloadThread(void)
{
	o_ProgressBar.DestroyWindow();

	if (p_Thread)
	{
		WaitForSingleObject(p_Thread->m_hThread, INFINITE);
		delete p_Thread;
		p_Thread = NULL;
	}
}

//**************************************************************************************************
bool PDDownloadThread::Download(PDDownloadUnit* pUnit)
{	
	if (FILE_EXISTS(pUnit->GetFile())) 
	{
		pUnit->SetDownloading();
		pUnit->SetComplete();
		return true;
	}

	//return _SocketDownload(pUnit);
	return _WinInetDownload(pUnit);
}


//**************************************************************************************************
bool PDDownloadThread::_WinInetDownload( PDDownloadUnit* pUnit )
{
	CString sTag; // Log tag
	sTag.Format("%09d-%09d", pUnit->GetStart(), pUnit->GetStart() + pUnit->GetSize());

	PDItem* pItem =  pUnit->GetItem();

	CString sProxyConn;
	if (pItem->e_Scheme == INTERNET_SCHEME_HTTP)
		sProxyConn.Format("http=http://%s:%d", CFG()->s_ProxyServer, CFG()->i_ProxyPort);
	else if (pItem->e_Scheme == INTERNET_SCHEME_HTTPS)
		sProxyConn.Format("https=https://%s:%d", CFG()->s_ProxyServer, CFG()->i_ProxyPort);

	//p_StatusBar->SetMsg("Opening internet connection.");
	HINTERNET hInet = InternetOpen("Part Downloader",
		//CFG()->b_UseProxy ? INTERNET_OPEN_TYPE_PROXY :INTERNET_OPEN_TYPE_PRECONFIG,
		INTERNET_OPEN_TYPE_PRECONFIG,
		CFG()->b_UseProxy ? sProxyConn: NULL, 
		NULL,0);

	//p_StatusBar->SetMsg("Connecting to internet.");
	HINTERNET hServer = InternetConnect(hInet, pItem->s_Server, pItem->i_Port,
		NULL, NULL, INTERNET_SERVICE_HTTP, 0, NULL);

	DWORD dMaxThreads = 7;
	BOOL bSOSuccess = 
		InternetSetOption(hServer, INTERNET_OPTION_MAX_CONNS_PER_SERVER, 
			&dMaxThreads, sizeof( dMaxThreads ));
	

	DWORD dwFlags = INTERNET_FLAG_KEEP_CONNECTION;
	if (pItem->e_Scheme == INTERNET_SCHEME_HTTPS)
		dwFlags |= INTERNET_FLAG_SECURE;

	CString sPath = pItem->s_FilePath;
	if (!pItem->s_ExtraInfo.IsEmpty())
		sPath += pItem->s_ExtraInfo;

	//p_StatusBar->SetMsg("Opening http request");
	HINTERNET hHttp = HttpOpenRequest(hServer, TEXT("GET"),
		//TEXT("/"),
		sPath,
		"HTTP/1.1", NULL, NULL, 
		dwFlags, 0);

	CString sExtraHeaders;
	sExtraHeaders.Format("Range: bytes=%d-%d", 
		pUnit->GetStart(), (pUnit->GetStart() + pUnit->GetSize() - 1));

	pUnit->SetDownloading();


	//{
	//	DWORD dwFlags;
	//	DWORD dwBuffLen = sizeof(dwFlags);

	//	InternetQueryOption (hHttp, INTERNET_OPTION_SECURITY_FLAGS,
	//		(LPVOID)&dwFlags, &dwBuffLen);

	//	dwFlags |= SECURITY_FLAG_IGNORE_UNKNOWN_CA;
	//	dwFlags |= SECURITY_FLAG_IGNORE_CERT_CN_INVALID;
	//	InternetSetOption (hHttp, INTERNET_OPTION_SECURITY_FLAGS,
	//		&dwFlags, sizeof (dwFlags) );
	//}

	BOOL bSuccess = HttpSendRequest(hHttp, sExtraHeaders, -1, NULL, 0);
	if (!bSuccess)
		return false;

	// Above will success after request is sent. Next is to read the response
	//////////////////////////////////////////////////////////////////////////

	int iResultBufSize = 10000;
	DWORD dwSize = iResultBufSize;	
	char* lpvResultHeaderBuffer = new char[dwSize];
	BOOL bRet = HttpQueryInfo(hHttp, HTTP_QUERY_RAW_HEADERS_CRLF,
		lpvResultHeaderBuffer, &dwSize, NULL);

	TRACE("%s\n", lpvResultHeaderBuffer);
	PDHTMLResponse* pRes = new PDHTMLResponse;
	if (!pRes->Init(lpvResultHeaderBuffer, dwSize))
	{
		InternetCloseHandle(hHttp);
		InternetCloseHandle(hServer);
		InternetCloseHandle(hInet);

		delete [] lpvResultHeaderBuffer;
		return false;
	}

	CString sTempFileName = pUnit->GetFile() + ".part";
	CFile oFile;
	CFileException ex;
	if (!oFile.Open(sTempFileName, CFile::modeWrite | CFile::modeCreate, &ex))
	{
		TCHAR szError[1024];
		ex.GetErrorMessage(szError, 1024);
		PDLog("[%s] File open error. [%s], err=%s", sTag, sTempFileName, szError);
		InternetCloseHandle(hHttp);
		InternetCloseHandle(hServer);
		InternetCloseHandle(hInet);
		return false;
	}

	DWORD dwPayloadSize = 20000;
	char* pPayLoadBuffer = new char[dwPayloadSize];
	DWORD dwPayloadRead = 0;
	BOOL bDataRes = InternetReadFile(hHttp, pPayLoadBuffer, dwPayloadSize, &dwPayloadRead);
	oFile.Write(pPayLoadBuffer, dwPayloadRead);
	pUnit->IncProgress(dwPayloadRead);

	*pPayLoadBuffer = 0;
	while (/*dwPayloadRead > 0 &&*/ pUnit->GetProgress() < pUnit->GetSize())
	{	
		BOOL bDataRes = InternetReadFile(hHttp, pPayLoadBuffer, dwPayloadSize, &dwPayloadRead);
		oFile.Write(pPayLoadBuffer, dwPayloadRead);
		pUnit->IncProgress(dwPayloadRead);
		*pPayLoadBuffer = 0;

		if (dwPayloadRead == 0)
			Sleep(100);

		if (pUnit->GetItem()->IsCancelled())
		{
			InternetCloseHandle(hHttp);
			InternetCloseHandle(hServer);
			InternetCloseHandle(hInet);
			return false;
		}
	}


	PDLog("[%s] Done data download complete.", sTag);
	oFile.Flush();
	oFile.Close();
	Sleep(500);

	InternetCloseHandle(hHttp);
	InternetCloseHandle(hServer);
	InternetCloseHandle(hInet);

	pUnit->SetComplete();

	PDLog("[%s] Renaming file .from=%s, to=%s", sTag, sTempFileName, pUnit->GetFile());
	CFile::Rename(sTempFileName, pUnit->GetFile());
	
	return true;
}

//**************************************************************************************************
bool PDDownloadThread::_SocketDownload( PDDownloadUnit* pUnit )
{
	CString sTag; // Log tag
	sTag.Format("%09d-%09d", pUnit->GetStart(), pUnit->GetStart() + pUnit->GetSize());

	struct hostent* pHost;
	unsigned int uiAddr;
	struct sockaddr_in saServer;

	PDItem* pItem =  pUnit->GetItem();



	int iPort = pItem->i_Port;
	CString sServer = pItem->s_Server;

	if (CFG()->b_UseProxy)
	{
		iPort = CFG()->i_ProxyPort;
		sServer = CFG()->s_ProxyServer;
	}

	SOCKET scConn;
	scConn = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if (scConn == INVALID_SOCKET)
		return false;

	if (inet_addr(sServer) == INADDR_NONE)
		pHost = gethostbyname(sServer);
	else
	{
		uiAddr = inet_addr(sServer);
		pHost = gethostbyaddr((char*)&uiAddr, sizeof(uiAddr), AF_INET);
	}
	if (pHost == NULL)
	{
		closesocket(scConn);
		return false;
	}

	saServer.sin_addr.s_addr = *((unsigned long*)pHost->h_addr);
	saServer.sin_family = AF_INET;
	saServer.sin_port = htons(iPort);
	if (connect(scConn,(struct sockaddr*)&saServer, sizeof(saServer)))
	{
		closesocket(scConn);
		return false;
	}


	// Preparing request
	char zRequest[HEADER_BUF_SIZE];

	if (CFG()->b_UseProxy)
	{
		sprintf_s(zRequest, HEADER_BUF_SIZE,
			"GET %s HTTP/1.1\r\n"
			"Host: %s\r\n"
			"Range: bytes=%d-%d\r\n"
			"Proxy-Connection: Keep-Alive\r\n\r\n"
			, pItem->s_URL, sServer
			, pUnit->GetStart(), (pUnit->GetStart() + pUnit->GetSize() - 1));
	}
	else 
	{
		sprintf_s(zRequest, HEADER_BUF_SIZE,
			"GET %s HTTP/1.1\r\n"
			"Host: %s\r\n"
			"Range: bytes=%d-%d\r\n"
			"Connection: Keep-Alive\r\n\r\n"
			, pItem->s_FilePath, pItem->s_Server
			, pUnit->GetStart(), (pUnit->GetStart() + pUnit->GetSize() - 1));
	}

	pUnit->SetDownloading();

	PDLog("[%s] Sending Header Request...\n%s", sTag, zRequest);
	// Sending request
	send(scConn, zRequest, strlen(zRequest), 0);
	PDLog("[%s] Header Request Sent...", sTag);


	// Receiving response
	PDLog("[%s] Getting Response...", sTag);
	char zResponse[HEADER_BUF_SIZE];
	int iResLen = recv(scConn, zResponse, HEADER_BUF_SIZE, 0);
	if (iResLen == SOCKET_ERROR)
	{
		PDLog("[%s] Socket Error: %d ", sTag, WSAGetLastError());
		closesocket(scConn);
		return false;
	}


	// Decoding response header
	PDHTMLResponse* pRes = new PDHTMLResponse;
	if (!pRes->Init(zResponse, iResLen))
	{
		PDLog("[%s] Error decoding request", sTag);
		closesocket(scConn);
		return false;
	}

	if (!pRes->IsPartialContent())
	{
		PDLog("[%s] Bad response. Not partial content.", sTag);
		closesocket(scConn);
		return false;
	}


	CString sTempFileName = pUnit->GetFile() + ".part";


	CFile oFile;
	CFileException ex;
	if (!oFile.Open(sTempFileName, CFile::modeWrite | CFile::modeCreate, &ex))
	{
		TCHAR szError[1024];
		ex.GetErrorMessage(szError, 1024);
		PDLog("[%s] File open error. [%s], err=%s", sTag, sTempFileName, szError);

		closesocket(scConn);
		return false;
	}

	// Add the remainder from initial request to this.

	pUnit->IncProgress(pRes->GetDeficitSize());
	oFile.Write(pRes->GetDeficitBuffer(), pRes->GetDeficitSize());
	PDLog("[%s] Writing remainder of the response. read=%d, rem=%d", sTag, iResLen, pRes->GetDeficitSize());


	char* zDataBuf = new char[CFG()->i_FetchBlockSize];

	fd_set fdReadSet; 
	FD_ZERO(&fdReadSet);
	FD_SET(scConn, &fdReadSet);
	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	int iStarveSkip = 0;

	while (pUnit->GetProgress() < pUnit->GetSize())
	{
		select(scConn + 1, &fdReadSet, NULL, NULL, &timeout);

		int iRead = recv(scConn, zDataBuf, CFG()->i_FetchBlockSize, 0);
		if (iRead == SOCKET_ERROR)
		{
			PDLog("[%s] Socket Error: %d", sTag, WSAGetLastError());
			delete [] zDataBuf;	
			zDataBuf = NULL;
			oFile.Close();
			closesocket(scConn);
			return false;
		}
		else
		{
			if (iRead > 0)
			{			
				pUnit->IncProgress(iRead);
				oFile.Write(zDataBuf, iRead);
			}

			if (iRead == 0)
				iStarveSkip++;

			if (iStarveSkip > 10)
			{
				iStarveSkip = 0;
				Sleep(1000);
			}
		}

		if (pUnit->GetItem()->IsCancelled())
		{
			PDLog("[%s] Exiting over cancellation. read=%d, out of=%d", sTag, 
				pUnit->GetProgress(), pUnit->GetSize());
			delete [] zDataBuf;	
			zDataBuf = NULL;
			oFile.Close();
			closesocket(scConn);
			return false;
		}
	}

	FD_CLR(scConn, &fdReadSet);

	PDLog("[%s] Done data download complete.", sTag);
	oFile.Flush();
	oFile.Close();

	PDLog("[%s] Renaming file .from=%s, to=%s", sTag, sTempFileName, pUnit->GetFile());
	CFile::Rename(sTempFileName, pUnit->GetFile());

	delete [] zDataBuf;
	zDataBuf = NULL;

	pUnit->SetComplete();
	return true;
}




//**************************************************************************************************
bool PDDownloadThread::IsCancelled()
{
	return false;
}

//**************************************************************************************************
void PDDownloadThread::SetUnit( PDDownloadUnit* pUnit )
{
	o_Lock.Lock();
	p_Unit = pUnit;
	if (pUnit)
	{
		PDLog("Setting range. start=%d, end=%d", 0, 100);
		o_ProgressBar.SetRange(0, 100);
		o_ProgressBar.SetPos(0);
	}
	o_Lock.Unlock();
}

//**************************************************************************************************
PDDownloadUnit* PDDownloadThread::GetUnit()
{
	PDDownloadUnit* pUnit = NULL;
	o_Lock.Lock();
	pUnit = p_Unit;
	o_Lock.Unlock();

	return pUnit;
}

//**************************************************************************************************
void PDDownloadThread::Execute()
{
	if (p_Thread)
	{
		WaitForSingleObject(p_Thread->m_hThread, INFINITE);
		delete p_Thread;
		p_Thread = NULL;
	}

	p_Thread = AfxBeginThread(TheadFunc, (LPVOID)this, 0, CREATE_SUSPENDED);
	p_Thread->m_bAutoDelete = FALSE;
	p_Thread->ResumeThread();
	//TheadFunc((LPVOID)this);
}

//**************************************************************************************************
UINT PDDownloadThread::TheadFunc( LPVOID pParam )
{
	PDDownloadThread* pDLThread = (PDDownloadThread*)pParam;
	PDDownloadUnit* pUnit = pDLThread->GetUnit();

	if (pUnit)
	{
		int iAttempts = 0;
		while (!pDLThread->Download(pUnit))
		{
			iAttempts++;
			PDLog("Download failed retrying...");
			if (iAttempts > CFG()->i_RetryCount)
				break;
		}
	}
	else
		TRACE("No Unit Set\n");

	return 0;
}

//**************************************************************************************************
void PDDownloadThread::Refresh()
{
	if (!p_Unit)
	{
		o_ProgressBar.EnableWindow(FALSE);
		o_ProgressBar.SetRange(0, DOWNLOAD_PROGRESS_RANGE);
		o_ProgressBar.SetPos(1);
				// 1 instead of 0, to indicate thread is ready but idle.
				// Otherwise 0 could indicate idle as well as stuck
	}
	else
	{
		o_ProgressBar.EnableWindow(TRUE);
		o_ProgressBar.SetBarColor(CLR_DEFAULT);

		int iPosition = p_Unit->GetProgress();
		iPosition = iPosition  * DOWNLOAD_PROGRESS_RANGE / p_Unit->GetSize();

		int iCurPos = o_ProgressBar.GetPos();
		if (iCurPos != iPosition)
			o_ProgressBar.SetPos(iPosition);
	}
}

//**************************************************************************************************
void PDDownloadThread::Clean()
{

}


//**************************************************************************************************
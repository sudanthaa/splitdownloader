#pragma once


class PDDialog;
// PDTileView

class PDTileView : public CWnd
{
	DECLARE_DYNAMIC(PDTileView)

public:
	PDTileView(PDDialog* pDlg, CRect rRect);
	virtual ~PDTileView();

	void	Reset();

	PDDialog* p_Dlg;
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};



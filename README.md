Split Downloader
==========

Helps you to skip proxy limits and download files.
Can be used to download files in multiple threads.

![splitdown.png](splitdown.png)
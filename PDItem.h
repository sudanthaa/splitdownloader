#pragma once

#include <list>
#include <WinInet.h>
#include "PDHTMLResponse.h"

class PDDownloadUnit;

#define  ITEM_STATE_STARTED		0
#define  ITEM_STATE_SIZE_CALC			1
#define  ITEM_STATE_SIZE_FOUND		2
#define	 ITEM_STATE_DOWNLOAD			3
#define	 ITEM_STATE_SIZE_FIND_ERROR		4
#define	 ITEM_STATE_CANCELLED		5


typedef std::list<PDDownloadUnit*> LST_UNIT;
typedef LST_UNIT::iterator LST_UNIT_ITR;

class PDItem
{
public:
	PDItem(void);
	~PDItem(void);

	bool	Init(const char* zURL);
	void	SetResponse(PDHTMLResponse* pResponse)	{	p_Response = pResponse;};
	PDHTMLResponse* GetResponse()	{ return p_Response;};
	int		GetSize();
	void	SetSize(int iSize)	{ i_Size = iSize;};
	void	AddUnit(PDDownloadUnit* pUnit);
	bool	HasIncomplete();
	PDDownloadUnit* PopIncomplete();
	void	AddCompletedUnit(PDDownloadUnit* pUnit);
	bool	IsAllComplete();
	LST_UNIT* GetUnits();
	void	Finalize();
	int		GetElapsed();
	void	Cancel();
	bool	IsCancelled();
	int		GetMergeState();
	void	SetMergeState(int iState);
	void	IncrementMergeState(int iValue);
	void	GetMergingFile(CString& sFile);
	void	SetMergingFile(const char* zFile);

	void	IncTotalProgress(int iBytes);
	int		GetTotalProgress();

	void	SetStatus(int iStatus);
	int		GetStatus();
	bool	IsSizeCalcError();
	bool	IsSizeFound();
	bool	IsDownloading();

	void	SetSizeCalcError();
	void	SetDownloading();
	void	SetSizeFound();

	CString s_ID;

	int		i_Status;
	int		i_Size;
	int		i_Port;
	INTERNET_SCHEME	e_Scheme;
	int		i_TotalProgress;
	int		i_MergeState;
	CString s_Server;
	CString s_FilePath;
	CString s_URL;
	CString s_EndFile;
	CString	s_MergingFile;
	CString s_ExtraInfo; // Get Parameters
	CTime	t_StartTime;

	LST_UNIT	lst_Units;
	LST_UNIT	lst_IncompleteUnits;
	LST_UNIT	lst_CompleteUnits;

	PDHTMLResponse* p_Response;

protected:
	CMutex  o_TotalProgressMutex;
	CMutex	o_Mutex;
	CMutex	o_MutexMegeState;
};